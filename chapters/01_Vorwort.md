# 1. Vorwort

## 1.1 Wer wir sind? Was wir wollen? Oh man, was ist passiert?
All das und noch viel mehr findet ihr in unserem gekürzten [Interview mit
dem Arbeitskreis E-Learning der LRK Sachsen](https://bildungsportal.sachsen.de/portal/digital-fellows-stellen-sich-vor-videos-in-der-lehre-das-mittel-der-wahl-fuer-flexibilisierung-und-individualisierung/) vom 30.01.2020 weiter unten.\

**Aber hier erstmal in Kurzfassung.**\
Wir sind drei Studis der TU Dresden und Mitglieder der
TU-Umweltinitiative (tuuwi). Die tuuwi ist schon uralt und ein
Zusammenschluss von (Nicht-)Studis irgendwo zwischen Hochschulpolitik,
Aktivismus und Umweltbildung - mit dem Ziel, den Campus, die
Studierenden und die Hochschulleitung hin und wieder an die
Dringlichkeit der Klimakrise und des Umweltschutzes zu erinnern. Gerade
letzteres stellt unseren Mittelpunkt dar: studentisch selbstverwaltete
und -organisierte Umweltbildung (FUßNOTE: tuuwi- Was wir machen).\
Deshalb haben wir Mitte 2018 an der Ausschreibung des
Digital-Fellowship-Programms, \[FUßNOTE: Erklärung DF\] eher aus Spaß an
der Freude teilgenommen - niemand hätte damit gerechnet, dass wir auf
einmal 12.000 Euro zur Digitalisierung unserer Umweltringvorlesungen
erhalten würden. Wir drei als einziges studentisches Projekt zwischen 40
weiteren Digital Fellows. WTF!\
Ziel war es von Anfang an, einen Leitfaden für andere studentische
Hochschulgruppen mit all unseren Erfahrungen zu erstellen und unser
Wissen niedrigschwellig zu teilen. Keine:r konnte damals ahnen, dass die
Digitalisierung der Hochschullehre im ersten Quartal 2019 durch die
Covid-19-Pandemie auf einmal so einen hohen Stellenwert, aber auch Schub
bekommen würde. Auch wenn sich in kürzester Zeit viele Fakultäten,
Lehrende und sicher auch Hochschulgruppen in der digitalen Welt
professionalisieren, möchten wir mit diesem Leitfaden über die Erfolge,
aber auch Herausforderungen einer digitalen studentischen Lehre
berichten und andere Studierende\
dazu befähigen, ihre (Lehr-)Projekte zu digitalisieren \[FUßNOTE:
Natürlich richtet sich der Text nicht nur an Studierende - gern können
alle, die sich angesprochen fühlen und Bock haben, Veranstaltungen
aufzuzeichnen, die Inhalte nutzen. Bitte beachtet hierbei, dass viele
Themen (auch in der Umsetzung) hochschulspezifisch und die
Veranstaltungsaufzeichnung an universitäre Strukturen angepasst ist.\]\
Dazu gehen wir zweiteilig in dem Leitfaden vor: In einem theoretischen
Teil wird zunächst auf die Grundlagen digitaler Lehre eingegangen. Dazu
spielen ein Verständnis von studentischer Selbstorganisation, Open
Educational Ressources (OER) und freier Software eine entscheidende
Rolle. Im Anschluss folgt der praktische Teil: Kommunikation, Aufnahme,
Schneiden, Bearbeiten und Veröffentlichen des Materials werden hier
grundlegend aus unserer Sicht erklärt und unsere Erfahrungen dazu mit
euch geteilt. Eine Zusammenfassung am Ende schafft einen Überblick;
ebenso wie der umfassende Anhang mit Checklisten, Software-Tipps und
Vorlagen für die Umsetzung. \
Wir wagen hiermit einen Spagat zwischen einem anwendungsorientierten
Leitfaden und einem hochschulpolitischen theoretischen Teil. Somit
werden teilweise zwei Seiten angesprochen: Studis und Engagierte sowie
Entscheider:innen und Verwaltung.

## 1.2 Interview mit dem Arbeitskreis E-Learning der LRK Sachsen:

**Digital Fellows stellen sich vor: Videos in der Lehre - Das Mittel der
Wahl für Flexibilisierung und Individualisierung? (30.01.2020)**\
*\"Videos sind vielseitig einsetzbar: Erklärvideos, aufwändige
Filmdokumentationen, Spielfilmauszüge, Vortragsaufzeichnungen oder
Videoprotokolle sind nur ein paar unterschiedliche Varianten. Können Sie
uns kurz erläutern, welche Art von Videos Sie einsetzen und wie Sie
diese in Ihr didaktisches Konzept einbinden?*\

Die studentische TU-Umweltinitiative (tuuwi) führt seit 30 Jahren
selbstorganisiert Umweltbildung in Form von Ringvorlesungsreihen und
anderen Lehrformaten an der TU Dresden durch. Die E-Learning-Plattform
OPAL wird bereits durch die Bereitstellung von Vorlesungsunterlagen,
sowie z.T. Audio-Mitschnitten und zur Lehrveranstaltungsverwaltung
genutzt. Mit Hilfe des Digital-Fellowship-Programms möchten wir jetzt
neue Möglichkeiten zum Bereitstellen der Veranstaltungsinhalte in Bild
und Ton schaffen.\
Die Vorträge werden von Gastreferierenden überwiegend nur für die
Vorlesungsreihe konzipiert und sind somit einmalig. Durch die
Aufzeichnungen werden sie dokumentiert, aufbereitet und archiviert und
so einem erweiterten Kreis von Studierenden zugänglich gemacht. \[\...\]
Die umfassende Dokumentation der Lehrinhalte schafft eine
Online-Wissensbasis im Sinne von Open Educational Resources im Bereich
der Umwelt- und Nachhaltigkeitsbildung.\
\
*Inwiefern unterstützen die Videos den Lernprozess Ihrer Studierenden?
Welche Rolle spielen diese in Bezug auf die Flexibilisierung und
Individualisierung des Lernprozesses?*\

Die tuuwi organisiert Umweltbildungsangebote mit dem Ziel, alle
Studierenden für Umwelt- und Nachhaltigkeitsthemen zu sensibilisieren
und mit wissenschaftlichen Fakten in Kontakt zu bringen. Somit
realisiert die tuuwi seit 2001 den Lehrauftrag der Kommission Umwelt
\[\...\]. Von einem Großteil der Studiengänge kann die Teilnahme an den
Lehrveranstaltungen mit der Vergabe von ECTS in das Studium eingebracht
werden. Der Videoeinsatz hilft den Studierenden vor allem bei der
Prüfungsvorbereitung, genau wie die Möglichkeit, alle Vorlesungsinhalte
jederzeit abrufen und nacharbeiten zu können. Außerdem ist eine
erfolgreiche Teilnahme an den Bildungsangeboten nicht mehr eine Frage
der physischen Anwesenheit. Das Projekt bietet allen Interessierten
einen niedrigschwelligen Zugang zu den Lehrinhalten. Dies ist in einer
Zeit, in der sich eine steigende Zahl an Menschen für Umwelt- und
Klimathemen interessiert, von Vorteil. Die Archivierung vergangener
Veranstaltungen kann darüber hinaus als Möglichkeit gesehen werden,
(Lern-) Inhalte zu verknüpfen, thematische Zusammenhänge zu erkennen und
das eigene Wissen zu vertiefen. Zusammengefasst profitieren die
Studierenden durch die zeitlich und örtlich unabhängige Verfügbarkeit
der Lehrmaterialien von einer individuellen sowie flexiblen Gestaltung
ihres Lernens; ihr Selbststudium und ihre Selbstlernkompetenzen werden
unterstützt \[\...\].\
\
*Was ist in Ihrem Fellowship das Innovative und Besondere am
Videoeinsatz?*\

Die Umweltringvorlesungen werden von der Konzeption bis zur
Klausurkontrolle von Studierenden durchgeführt. Hierbei stehen die
studentische Selbstorganisation sowie das Prinzip \"von Studierenden für
Studierende\" im Vordergrund. Die weiterführende Digitalisierung soll
eine \"Lehre für Alle\" ermöglichen und den Zugang zu den umfassend
gestaltenden Umweltbildungsangeboten niedrigschwelliger realisieren. So
werden besonders Menschen angesprochen, welche durch
bspw. familiäre Verpflichtungen, örtliche Distanz
oder körperliche Einschränkungen nicht an den Umweltbildungsangeboten
teilnehmen können -- es wird die Teilhabe an den wissenschaftlichen
Diskursen inklusiv gewährleistet \[\...\].\
Dennoch möchte ich nochmals betonen, dass in Zeiten der Klimakrise
besonders die Bildung zu umwelt- und klimarelevanten Themen zu einem
wachsenden Bewusstsein und letztendlich zur sozialökologischen
Transformation beitragen kann. Unsere Bildungsangebote werden als Teil
des *studium-generale*-Angebotes von Studierenden fakultäts- und
semesterübergreifend besucht und bieten den Studierenden die
Möglichkeit, die erworbenen Kompetenzen und das vermittelte Wissen
als Multiplikator:innen in ihre Studiengänge zu tragen.\
Darüber hinaus versuchen wir, unseren ökologischen Anspruch auch bei
diesem Projekt beizubehalten. Deswegen recherchieren wir verstärkt nach
möglichst nachhaltigen Speicher- und Hostingmöglichkeiten. Dazu zählt
zum Beispiel, dass die Server mit Strom aus erneuerbaren Energien
betrieben werden und durch Komprimierung und Verzicht auf unnötige
Softwarebausteine der Datendurchsatz gering gehalten wird.\
\
*Können Sie anderen Lehrenden noch weitere Hinweise und Erfahrungen zum
Videoeinsatz mitgeben?*\

Unser Projekt steht noch ganz am Anfang \[\...\]. Es empfiehlt sich
generell -- besonders was die Hardware betrifft -- zunächst bei der
eigenen Hochschule zu recherchieren. Die meisten Hochschulen besitzen
schon ein ausreichend ausgestattetes Medien- oder Ausleihzentrum; oder
wie bei der TU Dresden ein Zentrum für Vorlesungsaufzeichnung. Das
erleichtert die technische Materialbeschaffung ungemein! Im Laufe des
Förderzeitraums wird ein Leitfaden erarbeitet, der anderen
Hochschulgruppen und darüber hinaus die Nutzung digitaler Werkzeuge
erleichtert \[\...\]. Der Leitfaden soll öffentlich zugänglich und
universell anwendbar sein. Dort teilen wir dann alle Erfahrungen sowie
Tipps und Tricks rund um die (studentisch organisierte)
Vorlesungsaufzeichnung.