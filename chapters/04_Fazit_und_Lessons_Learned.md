**4. Fazit und Lessons Learned**

**4.1 Zusammenfassung**

Bei unserem Projekt zur Digitalisierung der URVs (Umweltringvorlesung)
haben wir einiges mitnehmen können, was wir euch hier zusammengefasst
mitgeben wollen.

**Studentische Selbstverwaltung und - organisation**

Bei studentischer Selbstverwaltung und -organisation geht es vor allem
um große Verantwortung, die aber gleichzeitig dafür sorgt, dass ein
großes Potential entsteht. \
Dabei ist die studentische Mitbestimmung ungemein wichtig und
wünschenswert. Schaut doch mal bei eurem StuRa oder auch ASTA vorbei, es
gibt immer wichtige Dinge, die erledigt werden wollen! Oder ihr wendet
euch direkt an die studentischen Vertreter:innen aus dem Senat? Ganz
egal wie, es lohnt sich immer.\
Darüber hinaus ist studentische Lehre eine wunderbare Möglichkeit, das
(doch mitunter veraltete) Lehrangebot ein wenig aufzufrischen. Es ist
möglich, eure Lehre selbst zu gestalten - Go for it! Dazu ist sie
eigentlich immer gerne gesehen, sowohl bei den Studierenden als auch bei
der Uni selbst.

**OER und freie Software**

Weiterhin ist OER (Open Educational Resources) ein wichtiges Konzept, um
Wissen zu teilen oder weiter zu geben. Konzepte wie „Lehre für Alle"
ermöglichen einen niedrigschwelligen und umfassenden Zugang zu
interessanten und gesellschaftlich relevanten Themen, wie
Umweltbildung!\
Hierbei bilden die CC (Creativ Commons) Lizenzen einen wichtigen
Baustein. Sie sorgen dafür, dass das Wissen und Einhaltung weniger
Punkte, wie das Nennen des Namens, frei zur Verfügung gestellt wird. Wir
empfehlen hier vor allem die verbreitete CC BY- SA 4.0 Lizenz. \
Bedacht werden muss, dass die Dozierenden meist nicht auf dem Gebiet
geschult und demnach von dem Konzept wenig überzeugt sind als auch den
Mehraufwand nicht auf sich nehmen wollen. Zur Not funktioniert hier auch
immer die Veröffentlichung auf internen Seiten eurer Uni, wie OPAL oder
Videocampus Sachsen in unserem Fall. Trotzdem sollte man es immer wieder
versuchen, um das Thema präsent zu halten. Dies funktioniert am besten,
indem man den Dozierenden die Umstrukturierung zu OER mit z.B. Leitfäden
und persönlicher Hilfestellung erleichtert.\
Auch freie Software sollte mehr verbreitet werden, um allen Personen den
Zugang zu ermöglichen. Außerdem sorgt sie für umfassende Transparenz und
Kontrolle über die eigenen Daten. Für die Veröffentlichung von Videos
gab es für uns folgende Kriterien: Erreichbarkeit, Verwaltungsaufwand,
Anwendungsmöglichkeiten, Anbieter der Plattform und deren Verwendung.
Eine Uni-interne Lernplattform bietet sich hier oft an, schließt aber
meistens externe Menschen aus. \
\
**digitale Lehre **

Was das Aufzeichnen in Präsenz oder auch digital angeht, ist es in der
digitalen Lehre besonders wichtig, sich rechtzeitig mit den zur
Verfügung stehenden Materialien oder Tools zu beschäftigen. Dozierende
und auch Studierende sollten rechtzeitig und vor allem transparent über
die Möglichkeiten informiert werden. Testläufe vor der Online Vorlesung,
wenn auch nur 10 Minuten vorher, empfehlen sich sehr. \
Auch das anschließende Bearbeiten sollte nicht auf die leichte Schulter
genommen werden. Holt euch bei all diesen Dingen unbedingt externe
Hilfe, insofern ihr diese benötigt. Universitäten haben meist ein
Zentrum zur Vorlesungsaufzeichnung oder ein Medienzentrum, welches euch
sicher gerne weiterhilft! \
Ansonsten ganz lapidar: einfach mal machen und sich trauen -
mittlerweile sind sowohl Studierende/euer Publikum, sowie die
Referierenden vorwiegend an die digitale Situation gewöhnt.\
\
**4.2 Lessons Learned**

**Was entwickelte sich positiv im Projektverlauf? **

-   schnelle Umstellung auf digitale Aufzeichnung von asynchroner und
    synchroner Lehre

-   Motivierte Dozierende, die nicht vor digitaler Lehre
    zurückschreckten

-   sehr gut vorhandene Technik beim Medienausleihzentrum und Zentrum
    für Vorlesungsaufzeichnung

-   BBB entwickelt sich als Tool ebenso schnell weiter - viel stabiler
    und nutzerfreundlicher

-   Digital Fellowship war eine sich kümmernde Ansprechperson

**Welche Herausforderungen ergaben sich bei der Projektdurchführung? **

-   verwehrter Zugang zu (rechtlichen) Wissen und finanziellen Mitteln,
    durch fehlenden Mitarbeiter:instatus

-   Aneignung rechtliches Wissen, da sehr unüberschaubar

-   Corona zwang uns auf digitale Lehre umzusteigen, deshalb sind andere
    Projektziele (wie die Implementierung von Untertiteln) verfallen

**Traten unerwartete Schwierigkeiten auf? Wenn ja, welche? **

-   überraschend wenig geschulte bzw. wissende Referierende über OER und
    Co.

    -   dadurch Ablehnung der Veröffentlichung auf z.B. Youtube

**Was würden Sie aus Ihren Erfahrungen heraus für ähnlich angelegte
Projekte empfehlen? **

-   frühzeitiges Klären von Ansprechpersonen und
    Unterschriftengeber:innen aus der Verwaltung sowie Tätigkeiten

-   rechtliche Aspekte nicht unterschätzen

-   vorhandene (technische) Infrastruktur nutzen!

**4.3 Forderungen**

Wir wären nicht die tuuwi, wenn wir nicht mit Forderungen diese
Broschüre abschließen. Wir verstehen Forderungen als ein legitimes
Mittel, um auf die uns begegneten Herausforderungen aufmerksam zu
machen.\
\
**Wir fordern\...**\
\
**1. Mehr Anerkennung und Empathie für studentische
(Drittmittel-)Projekte und studentisches Engagement!**\
Meisten sind wir als Studierende in das (Vollzeit- !) Studium und
Ehrenamt so eingebunden, dass vielfaches Nachfragen und schnelle
Bearbeitungen kaum (unbezahlt) möglich sind.\
\
**2. Konkrete Mentor:innen und Ansprechpartner:innen für Studierende zur
Verwaltung und Organisation von Drittmittelprojekten, aber auch für
andere von der TU \"anerkannte\" Projekte ohne Drittmittel.**\
Viel Zeit verging, um überhaupt verantwortliche Mitarbeiter:innen zu
finden. Durch in Forderung 1 genannte Hindernisse, erleichtern direkt
erreichbare Ansprechpersonen den Einstieg in die Realisierung des
Projekts und können auf spezifische studentische Herausforderungen
eingehen.\
\
**3. Einen niedrigschwelligen Zugang und Transparenz zu Wissen - nicht
nur mit Mitarbeiter:innen-Status.**\
Einige Informationen und Leistungen (Rechtsberatung, direkter Zugriff
auf Kosteneinsicht\...) wurden uns durch die hierarchische
Strukturierung des Verwaltungsapparates verwehrt und haben uns
einerseits bürokratischen Aufwand als auch Zeit gekostet. Hier könnte
die in Forderung 2 erwähnte Ansprechperson unterstützen, aber auch
Ausnahmeregelung auf Antrag durchführen.\
\
**4. Eine Sensibilisierung der Lehrenden und Referierenden seitens der
TU Dresden, zu dem Thema OER und digitale Möglichkeiten in der Lehre.**\
Viele Referierende sind zu wenig oder gar nicht hinsichtlich OER
aufgeklärt und reagieren verständlicherweise mit Unsicherheiten auf
Anfragen. Eine umfassende (rechtliche) Aufklärung können wir als
studentisches Projekt nicht leisten, deshalb sollte da die Uni ran - mit
bspw. niedrigschwelligen, gut erreichbaren (sowie verpflichtenden)
Schulungen, Workshops und Materialien, aber auch Bereitstellung
technischer Ausstattung.\
\
**5. Mehr finanzielle Unterstützung für die Verstetigung des Projekts
und damit der Durchführung und Aufbereitung von digitaler Lehre.**\
Aufgrund des hohen Aufwandes zu einer guten Bereitstellung von
Vorlesungsaufzeichnungen und Live-Streams braucht es festes Personal. In
der Mittelbeantragung für unseren Haushalt des gesamten
Umweltbildungsangebots für 2021/22 wurde eine Erhöhung abgelehnt.\