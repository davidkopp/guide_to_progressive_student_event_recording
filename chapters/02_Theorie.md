# 2. THEORIE: Streitschrift für freies Wissen, freie Software und studentische Selbsorganisation

## 2.1 Studentische Selbstverwaltung - (fehlende) Grundlagen des Arbeitens

>Eine Universität ohne Studierende verdient den Namen nicht. Die
Verwebung und Durchdringung von Lehrenden, die immer auch Lernende, und
Lernenden, die immer auch Lehrende sind, ist für die Universität
konstitutiv.

([https://www.hochschulverband.de/positionen/presse/resolutionen/eckpunktepapier-studentischen-mitbestimmung](https://www.hochschulverband.de/positionen/presse/resolutionen/eckpunktepapier-studentischen-mitbestimmung))\
\
Dieses Zitat des Eckpunktepapiers zur studentischen Selbstbestimmung des
Deutschen Hochschulverbandes soll als Einstiegsgedanke zu unserer Idee
und Erfahrung studentischer Mitbestimmung, Selbstorganisation und
-verwaltung dienen. Es bildet das Selbstverständnis der tuuwi, dieses
Projekts und definitiv vieler weiterer studentischen Lehrformate und
Hochschulgruppen ab.\
\
**Studentische Mitbestimmung **\
Studentische Mitbestimmung ist durch verschiedene formale Ämter, die 
bspw. während der jährlichen Uni-Wahlen erworben werden, gegeben. So
könnt ihr euch zum Beispiel in euren Fachschaftsrat (FSR) als
studentische Interessenvertretung eurer Fakultät wählen lassen
\[FUßNOTE: Die TUD erhebt im Rahmen einer Qualitätsanalyse regelmäßig
Studierendenbefragungen durch. Dort werden die Studierenden auch über
das Thema Mitbestimmungsrecht befragt. Leider konnten wir trotz
mehrmaligen Nachfragen (letzte Anfrage am 11.12.2020) keine Einsicht in
die Ergebnisse erlangen.\] Die Mitglieder des FSRs entsenden wiederum
Studierende in den Studierendenrat (oder andere hochschulspezifische
Gremienformate wie Asta oder StuPa) und schlagen die studentischen
Vertreter:innen für diverse Gremien der Fakultät vor.  Der FSR ist
häufig der Einstieg in die studentische Mitbestimmung - hier kann u.a.
die studentische Gremienarbeit beginnen. Demfolgend könnt ihr euch bspw.
in Gremien wie den Fakultätsrat, als höchstes und richtungsweisendes
Gremium eurer Fakultät, oder den Senat, als höchstes akademisches
Gremium der Universität, wählen lassen. Im Senat werden
richtungsweisende und grundlegende Entscheidungen für die gesamte
Universität bestimmt - und Studis können dabei sein! \[FUßNOTE: weitere
Gremien sind bspw. Bereichsrat, erweiterter Senat,
Gleichstellungsbeauftragte - informiert euch am besten bei eurem StuRa,
StuPa oder AstA!\]. Dazu kann der StuRa (StuPa, AstA) auch Studierende
in weitere universitätsinterne und -bezogene Gremien entsenden; in der
TU Dresden gehören dazu  bspw. die Kommission Umwelt oder der
Verwaltungsrat des Studierendenwerks.\
\
Exkurs: Keine Zeit und Kenntnisse für Demokratie - die neuste Studie des
correctiv Recherchezentrums (vgl. Lenz, Hering 2020) (FUßNOTE:
lesenswert und
weiterführend! [[https://correctiv.org/aktuelles/bildung/2020/08/09/uni-demokratie-studierende-waehlen-kaum/]{.ul}](https://correctiv.org/aktuelles/bildung/2020/08/09/uni-demokratie-studierende-waehlen-kaum/) )
zeigt, dass die Wahlbeteiligung von 70 deutschen Universitäten im Jahr
2019 durchschnittlich bei 13,6% lag! Die TU Dresden liegt immerhin auf
Platz 11 von 70 mit 20,7%. Ebenso finden sich kaum neue ehrenamtliche
Vertreter:innen sowie Orgnisator:innen. Warum? Nach den Autorinnen: Zu
wenig Zeit neben dem leistungsorientiertem Studium, zu viel
Verantwortung bei zu wenig Hilfestellung, kaum finanzielle
Aufwandsentschädigungen (vgl. ebd.).\
\
**Studentische Selbstverwaltung**\
Studentische Selbstverwaltung ist Ländersache, sodass es zu den
kommenden Ausführungen länderbezogene Abweichungen geben kann. Im
sächsischen Hochschulgesetz steht die studentische Mitwirkung unter §24
festgeschrieben: Die Studierendenschaft \"hat das Recht der
Selbstverwaltung im Rahmen der Gesetze\" und u. a. die Aufgabe, der
\"Förderung der politischen Bildung und des staatsbürgerlichen
Verantwortungsbewusstseins\" der Studierenden (vgl. SächsGVBl. S. 3,
2013). \"Bei der Wahl zum studentischen Rat oder Parlament geht es nicht
nur darum, sich an unserer Demokratie zu beteiligen. Es geht um Geld.
Viel Geld\" (Lenz, Hering 2020). So hat der StuRa der TU Dresden
jährlich über eine halbe Million Euro zur Verfügung (FUßNOTE: Nach dem
aktuellen Haushalt im Jahr 2019/20: 581.426,11 Euro einzusehen
hier [[https://www.stura.tu-dresden.de/webfm_send/2971]{.ul}](https://www.stura.tu-dresden.de/webfm_send/2971)).
Neben der hohen Verantwortung dieser studentischen Selbstverwaltung,
birgt diese auch unglaubliches Potential: Nur exemplarisch seien hier
die Finanzierung von Personalkosten, Beratungen,
(Bildungs-)Veranstaltungen und studentische Projekte (bspw. an der TUD
das festival:progressiv der Gruppe WHAT oder unseren Kohlekubus - eine
selbstgebaute Installation, die auf den konventionellen Stromverbrauch
der TUD aufmerksam macht \[LINKS\]) genannt. \
\
**Studentische Selbstorganisation**\
Von der bisher erwähnten studentischen Mitbestimmung, als auch
Selbstverwaltung, hängt vor allem die (nicht-) studentische
Selbstorgansation ab! StuRa und Co., FSRe und Hochschulgruppen (HSGs)
können selbstbestimmt Gelder beantragen, um so die bereits genannten
Potentiale zu realisieren. \
*Wie wichtig ist es, eigene studentische Projekte, Räume, Material usw.
zu verwirklichen und zu nutzen - unabhängig von Rektorat und
Hochschulleitung?* Diese rhetorische Frage soll auf die Relevanz und
Notwendigkeit der Ehrenamtlichen in Hochschulgruppen, Initiativen, StuRä
und Co. auf dem Campus hinweisen. Dazu ist hier auch ein großes
Dankeschön angemessen, da ihr den Campus und die Hochschule maßgeblich
zum Wohle der größten Gruppe an einer Hochschule - der Studierenden -
beeinflusst! Egal ob draußen bei eisiger Kälte am Glühweinstand oder
drinnen bei überlangen Sitzungen  im Senat.\
\
**Studentische Lehre und Bildungsangebote**\
In den aktuellen Qualitätszielen der TU Dresden für Studium und Lehre
steht: \"Der Studiengang sieht in den Studierenden wichtige
Partner:innen und fördert die studentische Mitwirkung\"(Stand
15.01.2020). Was also, wenn die Didaktik und Inhalte der Lehrenden von
gestern sind? Dann einfach selbst machen! Ein großer Teil unserer Arbeit
in der tuuwi ist unser studentisch selbstverwaltetes und -organisiertes
Umweltbildungsangebot: Von Umweltringvorlesungen bis zu Blockseminaren
und Projekttagen ist alles dabei. Als Studium-Generale-Angebote oder als
sogenannte autonome Seminare an euren Lehrstühlen sind diese Angebote
sogar im Studium anrechenbar! Erkundigt euch bei den jeweiligen
Studium-Generale AGs und Dozierende eures Vertrauens. Die Finanzierung
kann als Hochschulgruppe z.B. über euren StuRa und Co. laufen oder ihr
krallt euch eure studentische Senator:innen, damit sie
Finanzierungmöglichkeiten im Senat besprechen. \
Wir als tuuwi und langfristige Institution haben eine eigene
Kostenstelle, worüber wir jedes Jahr Gelder über die Uni-Verwaltung für
unser umfangreiches Umweltbildungsangebot ausschöpfen können - eine
großartige und privilegierte Position. Nur hört studentische Mitwirkung
leider bei der uniinternen Verwaltung auf. Als Studierende dürfen wir
auf Gelder, Kosteneinsichten und Co. nicht direkt zugreifen.
Insbesondere bei Drittmittelprojekten (wie dem
Digital-Fellowship-Programm) hatten wir kaum Handlungsmöglichkeiten,
aufgrund des hohen bürokratischen Aufwands und stark hierarchischer
Strukturen. Erst nach unzähligen \"Freischaltungen\" von Dritten in der
Verwaltung, konnten wir (viel zu spät) auf die Gelder des Projekt
zugreifen. Das muss niedrigschwelliger funktionieren!\
\
**Fazit** \
Ihr könnt an eurer Uni mitbestimmen und sie mitgestalten! Lasst euch
aufstellen oder wählt eure Vertreter:innen. Ändert Dinge, die euch nicht
passen - besonders in der Lehre! Erkundigt euch frühzeitig, wer
Ansprechperson für welchen Belang ist und fragt an. Lasst euch nicht von
bürokratischem Aufwand unterkriegen - übt euch in Geduld und
Freundlichkeit. Letztendlich wollen viele, auch in der Verwaltung, nur
das Beste für euer Anliegen. Studentisches Engagement ist meistens gern
gesehen!\
Gerade bei Drittmittelprojekten, die ihr als studentische Gruppen
organisiert, solltet ihr frühzeitig verwalterische Tätigkeiten,
Verantwortliche und Unterschriftengeber:innen lokalisieren und euch mit
wichtigen Ansprechpersonen verbünden. Fragen wie: 

-   Wo kommt das Geld hin? 

-   Wer verfügt darüber? 

-   Können wir als Gruppe darauf (frei) zugreifen? 

-   Wenn nicht, wer dann? 

-   Was muss bis wann feststehen und ausgegeben werden? 

sollten so früh wie möglich geklärt werden!\
\
Aber nun los geht\'s! Macht eure Uni ein Stück mehr zu eurer
Universität!

**2.2 Freies Wissen - OER**

Bildung sollte allen Menschen frei und kostenlos zugänglich sein. Ein
wichtiger Schritt in Richtung dieses Ziels ist das Festhalten und
öffentliche Teilen jeglicher Art von Wissen unter freier Lizenz.\
  \
Besonders in Zeiten der Klima- und Artenkrise führt Bildung über umwelt-
und klimarelevante Themen zu einem wachsenden Bewusstsein und kann so
letztendlich zur sozialökologischen Transformation beitragen. Gerade im
Rahmen unserer Umweltringvorlesungen (URV) wollen wir eine \"Lehre für
alle\" ermöglichen und den Zugang zu den umfassend gestaltenden
Umweltbildungsangeboten niedrigschwelliger realisieren. So werden
besonders Menschen angesprochen, welche durch bspw. familiäre
Verpflichtungen, örtliche Distanz oder körperliche Einschränkungen nicht
an den Umweltbildungsangeboten teilnehmen können. Es wird die Teilhabe
an den wissenschaftlichen Diskursen inklusiv gewährleistet. OER (Open
Educational Resources) ist dabei ein weit verbreiteter Ansatz zur
Umsetzung  dieses Ziels und wir möchten uns daran beteiligen.\
\
Die Ressource kann dabei sehr vielfältig sein -- von Übungsaufgaben über
Bücher bis hin zu Filmaufnahmen - kann prinzipiell jegliche Art Medium
als OER bereitgestellt werden.\
Damit die eigene Ressource als OER -- frei verfügbar-- veröffentlicht
werden kann, gilt es dabei, vor allem rechtlich einiges zu beachten. Im
Folgenden sollen die verschiedenen Aspekte von OER grob umrissen
werden. \
\
**2.2.1 Lizenzierung - Creative Commons**

Ein wichtiges Thema von OER ist die Wahl der Lizenz, unter der man das
eigene Material veröffentlichen möchte. Die Lizenz regelt, wer unter
welchen Bedingungen das eigene Material nutzen darf. Für OER sollte der
Verwendungsrahmen möglichst groß gesteckt sein, um eine einfache
Verbreitung und Wiederverwendung des Materials gewährleisten zu können.
Auch für diesen weiten Verwendungsrahmen existiert eine Vielzahl von
Lizenzen, die zur Veröffentlichung der Materialien als OER verwendet
werden können.\
\
An sich gibt es keine konkreten Vorgaben für die Wahl der Lizenz, sie
sollte natürlich immer unter dem Gesichtspunkt einer möglichst einfachen
Zugänglichkeit
geschehen.

Durchgesetzt haben sich dabei die sogenannten Creative Commons Lizenzen,
da sie recht einfach zu handhaben sind, unterschiedliche Einsatzzwecke
unterstützen und international einen rechtssicheren Umgang ermöglichen.\
Dabei gibt es eine große Bandbreite an CC-Lizenzen, die von einer
komplett freien Weiterverwendung bis zu einer starken Einschränkung der
Nutzungsrechte reichen. Eine spezifische CC-Lizenz wird aus
verschiedenen wählbaren Modulen, die durch Kürzel und entsprechende
Icons in der Lizenz dargestellt werden, zusammengesetzt. Jedes Kürzel
steht für eine bestimmte Eigenschaft der Lizenz. So bedeutet das Kürzel
BY in einer CC-Lizenz, dass bei der Weiterverwendung des lizenzierten
Materials der oder die Urheber:in genannt werden muss. Insgesamt erhält
man so eine kurze prägnante Lizenzformel aus Kürzeln und/oder Icons,
welche den genauen Umfang der CC-Lizenz beschreibt. \
\
Für OER sind dabei die folgenden drei CC-Lizenzen geeignet, da sie der
der geringen Beschränkung der Weiterverwendung von OER gerecht werden:

-    CC BY: Hier muss bei der Weiterverwendung der Name des/der
    Urheber:in genannt werden

    -   (Details zur Lizenz auf creativecommons.org).

-   CC BY SA: Hier muss bei der Weiterverwendung der Name des/der
    Urheber:in genannt werden. UND: Wenn ihr das Material remixt,
    verändert oder anderweitig direkt darauf aufbaut, dürft ihr eure
    Beiträge nur unter derselben Lizenz wie das Original verbreiten.

    -   (Details zur Lizenz auf creativecommons.org)

-   Außerdem gibt es noch die Möglichkeit, eigene Werke in
    die Gemeinfreiheit -- auch Public Domain genannt -- zu entlassen. Um
    das eindeutig zu kennzeichnen gibt es die CC0 (CC Zero)

    -   (Details dazu auf creativecommons.org)

Formatierungshinweis: Lizenzbilder einfügen =\> siehe Hub \
*\[Fußnote: Entnommen
von [[https://open-educational-resources.de/was-ist-oer-3-2/]{.ul}](https://open-educational-resources.de/was-ist-oer-3-2/) und
an eine Gendergerechte Sprache angepasst, sowie in der Ansprache der
Leser:innen verändert. Der Textteil steht unter CC BY 4.0-Lizenz. Der
Name der Urheberin soll bei einer Weiterverwendung wie folgt genannt
werden: Team OERinfo für OERinfo -- Informationsstelle OER.\]*\
\
Für uns hat sich die Creative Commons Lizenz CC BY- SA 4.0 als am
geeignetsten herausgestellt. Mit dieser Lizenz haben alle Interessierten
die Möglichkeit, sich die Videos der URV herunterzuladen und in ihren
eigenen Vorträgen und Projekten, unter Angabe des/der Urheber:in, zu
verwenden. Das Teilen von Wissen wird somit ungemein erleichtert.\
Letztendlich liegt die Entscheidung allerdings bei der vortragenden
Person, wie diese ihren Vortrag und alle damit verbundenen Materialien
lizenzieren möchte (mehr zu den unterschiedlichen Creative Commons
Lizenzen im Wikipedia Artikel).\
Für die einfache Überprüfung der OER-Tauglichkeit des eigenen Vortrags
haben wir eine Übersicht erstellt, welche wir den Vortragenden zur
Verfügung gestellt haben. \[Abb. x\]\
\
Mittlerweile finden sich im Internet viele gute Übersichten, Erklärungen
und Leitfäden rund um das Thema OER. Man muss nur wissen, auf welchen
Seiten sie zu finden sind. Umfangreiche Informationen findet man
unter [[https://open-educational-resources.de]{.ul}](https://open-educational-resources.de).
Auch das Bildungsportal-Sachsen hat eine ausführliche und verständliche
Broschüre zum Einsatz von OER
veröffentlicht: [[https://bildungsportal.sachsen.de/portal/wp-content/uploads/2019/01/Oersax_Broschuere_181212.pdf]{.ul}](https://bildungsportal.sachsen.de/portal/wp-content/uploads/2019/01/Oersax_Broschuere_181212.pdf)

**2.2.2 Schwierigkeiten bei der Lizenzierung **

Gerade durch die Zusammenarbeit mit vielen verschiedenen Doziernden im
Rahmen unserer URVen erwies sich die Bereitstellung der Aufzeichnungen
als OER als eine sehr komplexe Aufgabe.\
Nach Kontakt mit dem Team von OER-Info
([[https://open-educational-resources.de/]{.ul}](https://open-educational-resources.de/))
haben wir uns dazu entschieden, einen Autor:innenvertrag aufzusetzen.
Dieser sollte sicher stellen, dass der/die Urheber:in für die jeweilige
Aufzeichnung der Vorlesung, der Vorlesungsfolien und sonstigem Material
der Vorlesung die entsprechenden Nutzungsrechte einräumt, die eine
Verwendung als OER zulassen. Zudem wollten wir uns als Herausgeber:innen
für den Fall rechtlich absichern, dass eventuelle Rechteinhaber:innen an
verwendeten Materialen Ansprüche erheben sollten (z.B. auf Grund einer
falschen Verwendung ihrer Materialen).\
Unsere Version dieses Autor:innenvertrags findet sich im Anhang \[Anh.
x\]. Wir weisen darauf hin, dass wir den Vertrag selbstständig nach
einer Vorlage der TU München verfasst haben, deren Rechtssicherheit wir
nicht prüfen lassen konnten.* \[Fußnote: Mittlerweile ist ein Artikel
zur Erstellung eines Autor:innenvertrags auf
open-educational-resources.de
erschienen: [[https://open-educational-resources.de/autorenvertrag-fuer-oer/]{.ul}](https://open-educational-resources.de/autorenvertrag-fuer-oer/)\]*\
Für die Durchfürhung der URVen haben wir uns gegen die Möglichkeit
entschieden, die Wahl der Vortragenden an die Gegenzeichnung dieses
Vertrages zu knüpfen.\
Das Angebot einer URV mit spannenden Themen für den geschlossenen
Personenkreis von Teilnehmenden an der TU Dresden und damit die
Erfüllung des Lehrauftrags hat für uns Vorrang vor der Veröffentlichung
der Vorträge als OER. Aus diesem Grund haben wir in den Coronasemestern
als Alternative zum Autor:innenvertrag eine einfache
Einverständniserklärung zur digitalen Bereitstellung der Materialien in
einem, nur den Teilnehmenden der Veranstaltung zugänglichen Bereich (in
unserem Fall OPAL), angeboten (siehe Anhang X).\
Dabei haben sich nahezu alle Vortragenden auf Grund verschiedenster
Bedenken für die zweite Variante der Einverständniserklärung
entschieden.\
\
Folgende Bedenken wurden geäußert:\
1. Fehlende Rechte an in der eigenen Präsentation verwendeten Medien\
2. Sorge, dass Fehler bei der Verwendung von externen Medien gemacht
wurden und es zu einer Klage kommen könnte\
3. Der Umfang der im Vertrag eingeräumten Nutzungsrechte ist zu groß \
\
Punkt 1 und 2 haben wir versucht, mit unserem selbst erstellten Schema
zur Überprüfung, ob der eigene Vortrag OER geeignet ist, und weiteren
Tipps, wie z. B. den Verweis auf Suchmaschinen für freie Lizenzen
entgegenzuwirken. \
Allerdings scheint es hier noch an Erfahrung im Umgang mit Lizenzen und
einer Sensibilisierung für das Thema im Allgemeinen zu fehlen.\
Die ausschließliche Verwendung von freien Ressourcen gestaltet sich so
oft schwierig. Da die Vorträge häufig für einen geschlossenen
Personenkreis konzipiert wurden, wird darauf schlichtweg nicht geachtet.
Eine Überarbeitung der oft schon für verschiedene Vorträge verwendeten
Folien extra für die URV ist den Dozierenden meist zu komplex.\
\
In Punkt 3 sind wir auf Grund einer fehlenden Rechtsberatung *\[FUßNOTE:
als Studierende der TU steht uns keine Rechtsberatung zum Thema
Lizenzrecht zur Verfügung\] *leider nicht weitergekommen.  Auf Grund der
Dringlichkeit der Bereitstellung für die Studierenden mussten wir unser
Vorhaben für das digitale Semester deshalb leider auf Eis legen. Hier
wäre es auf jeden Fall wichtig mit entsprechender Hilfe einen
rechtssicheren Vertrag auszuarbeiten, bei dem die vortragende Person so
wenig Nutzungsrechte wie möglich einräumen muss und trotzdem eine
umfassende Verwendung der Materialien als OER ermöglicht wird. \
Weitere Informationen zu rechtlichen Fragen finden sich unter anderem in
folgendem
Leitfaden: [[https://irights.info/wp-content/uploads/2017/11/Leitfaden_Rechtsfragen_Digitalisierung_in_der_Lehre_2017-UrhWissG.pdf]{.ul}](https://irights.info/wp-content/uploads/2017/11/Leitfaden_Rechtsfragen_Digitalisierung_in_der_Lehre_2017-UrhWissG.pdf)

**2.3 Freie Software - Open Source Programme**

Freies Wissen geht für uns Hand in Hand mit der Verwendung von freier
Software. So wie die inhaltlichen Materialien frei verfügbar und für
jede:n nutzbar sein sollten, sollte das gleiche auch für die eingesetzte
Software gelten. Egal ob es darum geht, die Materialien betrachten,
erstellen oder bearbeiten zu können.\
\
Es wäre schlecht, wenn durch die Wahl der Software von vornherein
Menschen an der Verwendung, Bearbeitung des Materials oder auch
Nachahmung unseres Projekts ausgeschlossen werden. Dies tritt bspw. auf,
wenn für Programme Lizenzgebühren anfallen oder diese nur für bestimmte
Betriebssysteme verfügbar sind. Sowohl die Möglichkeit, Software ohne
Lizenzgebühren nutzen zu können, als auch in der Wahl des
Betriebssystems frei zu sein, sind positive Nebeneffekte von freier
Software. Im Kern geht es bei freier Software darum, diese transparent
und offen für alle zu entwickeln. Durch einen öffentlichen Quellcode und
entsprechende Lizenzierung freier Software wird die einfache und freie
Weiterverwendung und -entwicklung dieser gewährleistet. \
\
Der Freiheitsbegriff in diesem Kontext bedeutet, dass jede:r die
Freiheit der uneingeschränkten Kontrolle über die Software hat, diese
unabhängig von anderen  (z. B. von Firmen, die ihre Kund:innen gerne in
eine Cloud zwingen möchten - siehe Office 360) nutzen und den eigenen
Wünschen und Bedürfnissen entsprechend anpassen kann. Bei eingekaufter
Software passiert es häufig, dass die Abläufe an diese Software
angepasst werden müssen, weil gewisse Funktionen vom Anbieter der
Software so vorgegeben sind und nicht verändert werden können oder auch
die Anbindung von Tools anderer Anbieter nicht oder nur sehr schwer
möglich ist. \
Freie Software hingegen ermöglicht es, diese nach Bedarf an die eigenen
Bedürfnisse anzupassen. Häufig existieren auch Schnittstellen zu anderer
Software. So ist z. B. die kollaborative Schreibsoftware Etherpad direkt
in Meetingräume von BigBlueButton integriert.\
\
Eine weitere Komponente ist ebenfalls die Unabhängigkeit von
Unternehmen, deren Geschäftsmodell auf dem Sammeln und Auswerten von
Nutzer:innendaten zur Erstellung von perfekt auf Einzelpersonen
zugeschnittenen Werbeprofilen beruht. Freie Software ermöglicht es,
sich  der Datensammelwut von Google, Facebook und Co. und damit einer
dauerhaften Durchleuchtung  und einem Überwachungskapitalismus zu
entziehen und wieder Kontrolle über die eigenen Daten zu erlangen.\
\
Die Freiheit umfasst außerdem eine soziale Komponente. Jede:r ist frei,
sich selbst in die Entwicklung der Software einzubringen und mit den
Entwickler:innen und anderen Nutzer:innen direkt zu kooperieren. \
So entsteht im besten Fall eine Community um ein Projekt, die dieses
weiterentwickelt und z. B. Software für weitere Betriebssysteme und
damit eine größere Nutzer:innengruppe bereitstellt. Die Verfügbarkeit
des Quellcodes und die Möglichkeit, die Software zu kopieren, zu
verändern und weiterzugeben, ob im Original oder mit Veränderungen,
ebnen den Weg für kontinuierliche Verbesserung, Anpassung der Software
an die Bedürfnisse der Nutzer:innen und ein Fortbestehen der Software,
auch wenn die ursprünglichen Urherber:innen gar nicht mehr am Projekt
beteiligt sind. \
Natürlich passiert das alles nicht von allein, die Weiterentwicklung
freier Software erfordert viel Unterstützung durch freiwillige
Investition von Zeit und/oder Geld, damit dieses Modell funktionieren
kann.\
Es benötigt Menschen, die die Software weiterentwickeln und die
Ressourcen, dies zu tun. Deswegen möchten wir an dieser Stelle dazu
aufrufen, freie Software nicht nur zu nutzen, sondern, durch
Unterstützung der verwendeten Projekte auch etwas zurückzugeben. \
Die Möglichkeiten zur Unterstützung sind dabei sehr vielfältig. Viele
Projekte finanzieren sich über Spenden, um die Arbeitszeit, die in das
Projekt fließt, entlohnen zu können, aber auch, um die Bereitstellung
von Ressourcen bezahlen zu können. Ein guter Ansatz ist es hier z. B.,
einen Teil des durch den Einsatz freier Software gesparten Lizenzgeldes
an die Projekte hinter der eingesetzten Software zu spenden. Ebenso
hilft es, auftretende Fehler zu dokumentieren und an die
Entwickler:innen weiterzureichen. Oder man wird gleich selbst aktiv und
hilft bei der Übersetzung und Gestaltung der Anwendungsoberfläche in
verschiedene Sprachen, Umsetzung neuer Features usw. Auch der bewusste
Einsatz von freier Software anstatt bekannter proprietärer, also nicht
frei zugängliche, Software trägt zu einer höheren Verbreitung und damit
auch zu mehr Unterstützung bei.\
Dabei haben viele Projekte freier Software eigene Webseiten, über die
man schauen kann, wie man ihnen am besten helfen kann. 

**Warum wir freie Software einsetzen und empfehlen**

-   allen den einfachen Zugang zur Software ermöglichen, unabhängig von
    verwendetem Betriebssystem, Alter des eingesetzten Gerätes und
    Geldbeutel

-   ermöglicht unter anderem einfaches Nachahmen dieses Leitfadens

-   bewusste Abkehr von Geschäftpraktiken, die auf Überwachung der
    Nutzer:innen bzw. dem massenhaften Sammeln von Daten aufbauen =\>
    Bestimmung über die eigenen Daten zurück in die Hände der
    Nutzer:innen legen

-   die eigenen Materialien frei von kommerzieller Werbung anbieten

-   zur weiteren Verbreitung von freier Software beitragen und die
    verwendeten Softwareprojekte unterstützen

-   ermöglicht es uns, nach Bedarf auch eigene Dienste anzubieten, die
    Software an die eigenen Abläufe anzupassen und mit einander zu
    verbinden