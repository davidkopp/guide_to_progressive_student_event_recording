# 3. PRAXIS: Praktischer Teil - Leitfaden

## 3.1 Das Projekt in Kürze - die eigentliche Idee

Mit Hilfe des Digital-Fellowship-Programms sollten neue Möglichkeiten
zum Bereitstellen der Veranstaltungsinhalte in Bild und Ton geschaffen
werden. Dazu gehörten die technische Umsetzung, sowie die Klärung der
rechtlichen Voraussetzungen. Zusätzlich sollten die Videos mit
Untertiteln in deutscher und englischer Sprache ergänzt werden.
Weiterhin wurde angedacht, eine eigene Plattform zur Bereitstellung der
Inhalte zu implementieren. Abschließend sollte ein Leitfaden erstellt
werden, wie sich die beschriebenen Vorhaben auf andere (studentische)
Lehrveranstaltungen übertragen lassen.   \
\
**Ablauf**\
**1.  Phase -- Planung**: Projektplan & Zusammenstellung des
Projektteams\
**2. Phase -- Recherche & Test:** IST-Zustand ermitteln durch u. a. 
Testlauf\
**3. Phase -- Auswertung Test:** Evaluation,  SOLL-Zustand ermitteln &
Fehlendes ausgleichen\
**4. Phase -- Durchführung:** Aufnahme, Bearbeitung &  Hochladen einer
Ringvorlesung\
**5. Phase -- Leitfaden:** Erstellung & Veröffentlichung des Leitfadens\
**6. Phase -- Evaluation:** Abschluss des Projekts &  Förderungsende \
\
**Coronabedingte Änderungen und Ergebnis**\
Durch die Covid-19-Pandemie im ersten Quartal 2020 wurden sowohl unsere
Umweltbildungsangebote, als auch das Projekt auf den Kopf gestellt. Die
ursprünglich geplanten Präsenzvorlesungen und deren Aufzeichnungen
fielen aus, sodass wir uns relativ spontan auf asynchrone sowie
synchrone Online-Lehre und deren Aufbereitung fokussierten. Demnach
konnten wir einige hochgesteckte Ziele nicht verwirklichen. Trotz alldem
haltet ihr hier unser höchstes Ziel in den Händen: Im Förderzeitraum
sollte ein Leitfaden für den sächsischen Hochschulraum erarbeitet
werden, der anderen Hochschulgruppen (und darüber hinaus) die Nutzung
digitaler Werkzeuge erleichtert. Dazu gehört für uns eine umfassende
Dokumentation aller technischen (und rechtlichen) Anforderungen,
eventueller Schwierigkeiten und Möglichkeiten der digitalen
Nachhaltigkeit sowie ein öffentlicher Zugang für alle Interessierte.

**(nicht-) erreichte Ziele**

-   öffentlich zugängliches, dauerhaft erreichbares und gut
    aufbereitetes Umweltbildungsmaterial (viertel CHECK - z. B. keine
    Untertitel, Veröffentlichung nur mit versteckten Links)

-   verständliche Möglichkeiten zur Umsetzung der Digitalisierung von
    Lehrveranstaltungen (CHECK)

-   zeit- und ortsunabhängige Teilhabe an den
    Umweltbildungsveranstaltungen und eine „Lehre für
    Alle" (dreiviertel CHECK)

-   Schaffung einer Online-Wissensbasis im Sinne von Open Educational
    Resources im Bereich der Umwelt- und Nachhaltigkeitsbildung (NOPE)

-   Auseinandersetzung mit digitalen Möglichkeiten von Lehre und
    Förderung des interdisziplinären Austausches der Studierenden
    (CHECK)

-   digitale Unterstützung bei der Vorlesungsnachbereitung und
    Klausurvorbereitung und Aufbau digitaler Kompetenzen auf Seiten der
    Studierenden und Lehrenden (CHECK)

-   Vernetzung von Hochschulgruppen und Initiativen an der TUD und
    anderen sächsischen Hochschulen (NOPE)

-   eigene (Umweltbildungs-)Plattform (WEIT ENTFERNT)

In den kommenden Kapiteln wird nun Schritt für Schritt die Durchführung
einer onlinebasierten Lehrveranstaltung vorgestellt und mit unseren
Erfahrungen verknüpft. Der vorliegende Praxisteil beachtet dabei
wahrscheinlich nicht alle Möglichkeiten. Aber vor allem die, in unserem
Rahmen möglichen, Tipps und Tricks der Realisierung von Online-Lehre.
Ergänzungen und Anpassungen sind freilich möglich und äußerst
wünschenswert.

**3.2 Kommunikation mit den Referierenden**

**Wie war es bei uns?**\
Bei unserer Kommunikation mit den Dozierenden muss unterschieden werden
zwischen dem ersten und dem zweiten Semester, in denen unser Projekt
durchgeführt wurde. Im ersten Semester mussten wir aufgrund der akuten
Coronasituation die Kommunikation etwas ändern. Dabei hat der
Mailaustausch deutlich zugenommen, da es zu diesem Zeitpunkt für fast
alle Vortragenden relativ neu war, Vorlesungen digital anzubieten.
Außerdem wurden wir überwiegend mit Skepsis hinsichtlich der allgemeinen
Veröffentlichung\
 konfrontiert (siehe Kapitel \"Schwierigkeiten bei der Lizenzierung\").\
Im Laufe des zweiten Semesters wurden ein paar Dinge verändert.
Die Dozierende sollten jetzt keine Entscheidung mehr treffen, ob sie ihr
Video nur intern (auf OPAL) oder ganz veröffentlichen wollen. Wir
konzentrierten uns jetzt nur auf die interne Variante, da letztere
leider durchgehend abgelehnt wurden. Die Technik, die sich im ersten
Semester gut bewährte, wurde auch im zweiten Semester wieder benutzt.
Konkret wurden die Vorlesungen auf GoToMeeting und/oder über
BigBlueButton gehalten und aufgezeichnet. Die Auswahl der Tools wurde je
nach Teilnahmekapazitäten abgewogen. Die Referierenden schienen dabei
mit dem allgemeinen Ablauf, den Tools und Co. sichtlich vertrauter.\
\
**Worauf sollte geachtet werden?**\
Mails an die Referent:innen sollten Informationen über die (veränderte)
Organisation, den Ablauf, technische Tools enthalten, aber auch
transparent hinsichtlich des Digitalisierungsvorhabens sein. \
Das Ziel war, den Dozierenden eine Hilfestellung fürs Aufzeichnen zu
geben und sie von der OER-Thematik zu überzeugen bzw. wenigstens zu
motivieren, ihre Vorlesungen unter einer CC- Lizenz zu
veröffentlichen. \
Um dies zu erreichen, helfen übersichtliche selbst erstellte Leitfäden
zum Aufzeichnen mit OBS (siehe Anhang X) oder kleinere
„Motivationstexte", die OER als Idee vorstellen und anpreisen.
Zusätzlich dazu unterstützen Leitschemata, mit deren Hilfe man bestimmen
kann, ob der Vortrag oder dessen Video unter einer CC- Lizenz
veröffentlicht werden kann. Dies sollte v. a. die konkrete Entscheidung
für oder gegen freie Lizenz erleichtern. \
Darüber hinaus darf eine Einverständniserklärung nicht fehlen, damit die
Videos veröffentlicht werden können. Hier ist eine zweiteilige Variante
hilfreich: eine, bei der das Video nur auf der TUD-internen
Lernplattform OPAL erscheint und eine, bei der wir stellvertretend das
Video auf einer Plattform wie Youtube veröffentlichen. Jede Sensibilität
und Aufklärung hinsichtlich OER und freizugänglichen Veröffentlichungen
helfen den Dozierenden, mehr Sicherheit zu erlangen. Nutzt dazu gern
unseren umfangreichen Anhang!

**3.3 Aufzeichnen**

Da es eigentlich geplant war, Präsenzvorlesungen aufzuzeichnen, gehen
wir an dieser Stelle sowohl auf Präsenz- als auch Onlinelehre ein.\
\
**Präsenzlehre:**\
Folgendes bezieht sich weniger auf die Technik, Geräte und Ausführung,
als auf die Dinge, die es drumherum zu beachten gilt. Außerdem wollen
wir betonen, dass wir bei Weitem nicht alles über den Umgang mit Kameras
usw. wissen und das Folgende nur Empfehlungen aus persönlichen
Erfahrungen sind. Wenn ihr mehr wisst oder eine bessere Idee habt, do
it! \
Trotz allem sind einige technische Geräte notwendig, um Vorlesungen
aufzeichnen zu können. Diese kann man sich bei den meisten Universitäten
an einer dafür vorgesehenen Stelle ausleihen \[Fußnote: Medienzentrum
TUD und Zentrum für Vorlesungsaufzeichnung\]. In unserem Fall ging dies
sehr unkompliziert, allerdings sollte man sich rechtzeitig bei der
vorgesehenen Stelle melden, da meist schon nach Beginn des Semesters
alle Geräte vergeben sind. \
Wir verfügten über eine Kamera plus Stativ, ein Mikrophon für die
Dozierenden, außerdem einen externen Screen Recorder und verschiedenste
Kabel, um das Video und Tonsignal zu übertragen. Der Screen Recorder ist
besonders praktisch, da er sich an den Videoausgang eines beliebigen
Geräts anschließen lässt und die Präsentation schnell und unkompliziert
aufzeichnet. Er hat ebenfalls eine SD-Karte und lässt sich nach dem
Anschließen eines Mediums über einen Startknopf aktivieren.  \
Extra-Tipp: Es macht sich übrigens hervorragend, einen Rucksack für alle
Sachen zu haben. Dann sind alle technischen Geräte an einem Ort und ihr
könnt schnell auf sie zugreifen.\
Die Kamera ist dazu gedacht, die vortragende Person aufzuzeichnen,
während der Screen Recorder die Aktivitäten auf dem Laptop erfasst. Am
Ende werden beide Videos zusammen gezeigt. Die Videos lassen sich bspw.
so anordnen, dass die Präsentation groß und die Dozierenden als kleines
darüber gelegtes Video in einer Ecke zu sehen sind, aber auch eingebaute
Wechsel zwischen den Videospuren sind denkbar -- einfach ausprobieren,
was am besten zum Vortrag passt. \
\
Was die Aufzeichnung mit Kamera angeht, sollte man auf folgendes achten:

-   nicht gegen ein Fenster filmen

-   nicht seitlich im Hörsaal stehen

-   Das Licht im Hörsaal muss hell genug sein -- gerade Beamer sind
    häufig sehr grell.

-   Alle Objekte, die das Bild stören, sollten aus dem Weg geräumt
    werden und möglichst wenig Zuhörende im Bild sein. 

-   Das Filmen an sich bedarf auch etwas Übung, da man die Dozierende
    nicht zu klein filmen sollte. Gleichzeitig bewegen sich manche
    Dozierende sehr viel, sodass man die Kamera oft bewegen muss. Hier
    eignet es sich, etwas heraus zu zoomen, auch wenn am Ende die
    gefilmte Person nur sehr klein zu sehen ist. Im besten Fall sprecht
    ihr dies mit euren Referent:innen ab! Da ist also Abwägen nötig.

**Was es sonst so zu beachten gibt:**\
Es ist wichtig, immer ca. 15 bis 20 Minuten vor Beginn der Veranstaltung
da zu sein, um die Kamera aufzubauen, alle Kabel anzubringen, das Licht
einzustellen, die Anlage im Hörsaal eventuell freizuschalten und
mögliche technische Probleme zu lösen, die leider irgendwie (fast) immer
auftreten. Wenn es keinen geeigneten Platz im Hörsaal neben einer
Steckdose gibt, empfiehlt sich ein Verlängerungskabel. Außerdem müssen
die Dozierende ihr Mikrophon selbst anbringen, da man ihnen sonst
schnell zu nahekommen kann.\
Es empfiehlt sich, am Beginn einmal vor der Kamera zu schnipsen oder zu
klatschen, um Ton und Bild im Nachhinein synchronisieren zu können.
Ansonsten sollte immer darauf geachtet werden, dass die Batterien der
Mikrophone genug Kapazität haben und genug Speicherplatz auf der
Speicherkarte der Kamera ist. \
Während der Vorlesung sollte man mehrmals überprüfen, ob die Technik
funktioniert und sich eine Lösung überlegen, wie mit Fragen umgegangen
wird, da das Publikum im Normalfall (aus rechtlichen Gründen die bessere
Wahl) nicht mitgefilmt wird. Es ist zwar rechtsgemäß, das Publikum als
Ganzes zu zeigen, nicht aber mit dem Fokus auf einer Einzelperson. Eine
Lösung ist bspw., das Notieren der gestellten Fragen, um diese am Ende
separat in das Video einzufügen. Alternativ kann auch ein separates
Mikrophon zum Aufnehmen der Fragen herumgeben werden. \
Soweit die Dinge, die uns bei unseren Testläufen ***vor*** der Pandemie
aufgefallen sind. In einem realen Szenario fallen sicherlich noch
weitere Probleme und ganz sicher deren Lösungen auf. Es empfiehlt sich,
diese auf jeden Fall gut zu dokumentieren, damit dieses Wissen
weitergegeben werden kann. \
 \
**Online-Lehre:**\
Was die synchronen Online Vorlesungen angeht, so wollen wir hier eher
ein paar kurze Tipps zur allgemeinen Organisation geben und weniger auf
die Aufzeichnung eingehen. \
\
Die Aufzeichnung kann meist über den Button \"Aufnahme starten\" bei BBB
oder GoToMeeting gestartet werden. Die Tools zeichnen dann im
Hintergrund alles auf und benachrichtigen euch nach einer gewissen
Konvertierungszeit (abhängig von Länge der Aufzeichnung) per Mail mit
der fertigen Datei. \
Bei sowohl asynchroner Online-Lehre, als auch bei Videochatsoftwares,
bei der es keinen Aufnahmebutton gibt, empfiehlt sich OBS für die
Aufzeichnung (umfassender Leitfaden im Anhang). Wir haben uns
entschieden, sofern es geht, \"online\"-Aufzeichnung und OBS
gleichzeitig zu benutzen als zusätzliche Sicherheit und empfehlen dies
auch weiter. \
\
Die Referent:innen sollten über die verfügbaren Optionen informiert
werden, es gibt im Wesentlichen zwei:\
Die, die wir vorwiegend im ersten Online-Semester benutzt haben, ist die
asynchrone Vorlesung. Hier wird die Vorlesung im Vorhinein aufgenommen
und dann auf einer Plattform den Studierenden zur Verfügung gestellt.
Die zweite Möglichkeit ist die synchrone Vorlesung. Hier wird die
Vorlesung live gehalten. Der große Vorteil ist die Interaktion zwischen
Dozent:in und Publikum. Allerdings bedarf dieses Format auch ein wenig
mehr Vorbereitung. \
Im Vorhinein sollte ein Raum über eine Plattform wie BBB erstellt
werden, der anschließend für die Vorlesung genutzt wird. Außerdem
sollten die Dozierenden informiert sein, wie die wichtigen Einzelheiten
des Tools funktionieren. Selbstverständlich sollte immer mindestens eine
Person die Vorlesung einleiten bzw. betreuen. Darüber hinaus müsst ihr
euch überlegen, wie ihr mit dem Chat umgehen wollt. Soll eine Person
sich um den Chat kümmern und im richtigen Moment Fragen einstreuen oder
wollt ihr bspw. am Ende oder nach einem Abschnitt die angefallenen
Fragen durchgehen? \
Am besten empfiehlt sich hier ein Treffen/Absprache vorab. Ein weiterer
Punkt ist die Information der Studierenden. Es macht sich gut, bevor die
Vorlesungsreihe startet, eine Mail an alle Zuhörer:innen zu schicken,
die wichtige Infos enthält (z. B. über die Möglichkeiten des
Leistungspunkteerwerbs oder den allgemeinen Ablauf). Zusätzlich sollten
die Studis kurz vor der Veranstaltung noch einmal auf den kommenden
Termin aufmerksam gemacht werden, damit auch wirklich so viele wie
möglich erscheinen. 

**3.4 Schneiden, Bearbeiten**

An dieser Stelle soll es nicht darum gehen, wie das Video geschnitten
wird o. Ä. Stattdessen soll kurz zusammengefasst werden, welche
Bearbeitungsschritte aus unserer Sicht sinnvoll sind. Das Know-how der
Umsetzung kann dann relativ einfach aus Tutorials im Internet erlernt
werden \[FUSSNOTE: weiterführende Links unbedingt\]. \
\
Als Tool zum Bearbeiten und Schneiden der Videos empfehlen wir Shortcut,
da es kostenlos ist und alle grundlegenden Funktionen besitzt. Wenn man
eine Ton- und Bildspur hat, sollten diese als erstes übereinandergelegt
werden und anschließend an den gewünschten Stellen Material entfernt
werden. Weiterhin kann z.B. die Qualität und Anzahl der Bilder pro
Sekunde runtergestellt werden, um so die Größe der Datei erheblich zu
verkleinern. Dies erleichtert das Hoch- und Runterladen und die Qualität
des Videos wird nicht allzu sehr verschlechtert. Hier empfiehlt sich am
Anfang das Ausprobieren mit einem kleinen Teil des Videos. Welche
Einschnitte der Qualität für das gewünschte Format am besten geeignet
sind, könnt ihr selbst entscheiden. Der Vorgang dauert zwar etwas
länger, aber anschließend ist das Video deutlich kleiner in der
Dateigröße und verkürzt die Konvertierungs- und Ladezeiten erheblich.
Darüber hinaus empfehlen wir für die Komprimierung das Programm
\"handbrake\", welches sich besonders durch seine einfache Handhabung
auszeichnet. \[FUSSNOTE: Link\]: Bereits in der Grundeinstellung des
Programms können Videos mit wenigen Klicks merklich verkleinert werden
ohne merkbare Qualitätsverluste. \
Am Anfang und Ende kann man meist das Video noch ein wenig
zurechtschneiden, um überflüssige Teile des Videos zu entfernen. Auch
technische Ausfälle oder andere \"unwichtigen\" Teile können
weggeschnitten werden. Je nachdem wie das Layout sein soll, gibt es auch
die Möglichkeit, einen Hintergrund einzufügen (siehe OBS Leitfaden im
Anhang X). Prinzipiell empfiehlt sich eine Anordnung, in der die
Vortragenden kleiner in einer Ecke sind und der Inhalt des Vortrages
mittig und groß positioniert ist. Titelbilder und Untertitel können nach
Belieben hinzugefügt werden. Wenn all dies geschehen ist, gilt auch hier
wieder: so schnell wie möglich hochladen. U.U. verlieren Studierende die
Lust auf die Vorlesungsreihe, wenn die Videos erst geraume Zeit nach den
eigentlichen Veranstaltungen erscheinen. Aus Erfahrung bietet es sich
an, die Aufzeichnung ein bis zwei Tage nach der Live-Vorlesung
anzukündigen (Konvertierungszeit, Bearbeitungszeit etc.). 

**3.5 Veröffentlichen**

Die Veröffentlichung ist der letzte Schritt auf dem Weg zur eigenen
(OER-)Veranstaltung. Folgende Entscheidungen gilt es, zu treffen:

1.  Die Wahl der Plattform, auf der man die Materialien hochladen und
    damit anderen zur Verfügung stellen möchte. 

2.  Der Upload-Ort ist so zu wählen, dass möglichst viele Menschen
    diesen finden und möglichst einfach Zugriff auf die Materialien
    erhalten. Dazu zählt auch, dass ein Herunterladen der Materialien
    einfach möglich ist. 

3.  Der Verwaltungsaufwand: wie viel Arbeit muss investiert werden, um
    bereitgestellte Informationen und die Plattform selbst aktuell zu
    halten?

Im Laufe des Projekts haben wir uns folgende Möglichkeiten der
Veröffentlichung angeschaut:

-   Nutzung von Portalen der TU Dresden bzw. des Freistaates Sachsen

    -   OPAL

    -   Videocampus Sachsen

-   Eigene Webseite / Videoplattform

    -   tuuwi.de

    -   eingekaufte kommerzielle Plattform

    -   eigene Peertube-Instanz

-   Öffentliche Videoplattform

    -   Youtube

    -   öffentliche Peertube-Instanz

**Gegebenheiten an der TU Dresden**

Über die TUD haben wir die Möglichkeit, Angebote des Bildungsportals
Sachsen zu nutzen. Für uns interessant sind dabei vor allem OPAL, als
umfassende Lern- und Kursplattform und der Videocampus Sachsen als
Videoplattform, die die Einbindung der Videos direkt in einen eigenen
OPAL-Kurs ermöglicht.

**OPAL**

Für Einschreibung und die Kommunikation mit den Teilnehmenden nutzt die
URV-Koordination seit jeher OPAL. \
Für jede Veranstaltung wird ein eigener OPAL-Kurs angelegt, in den sich
Interessierte über ihren TUD-Account oder mit einem Gastzugang
\[FUßNOTE: Link zu Anleitung\] einschreiben können. Innerhalb dieses
Kurses lassen sich Mitteilungen an alle Eingeschriebenen versenden,
Materialien bereitstellen und verschiedene Module, wie z. B. ein Forum
hinzuschalten. Im ersten Coronasemester (Sommersemester 2021) haben wir
Materialien im OPAL-Kurs hochgeladen, damit die Vorlesungen zum Download
bereitstehen. Wie im Kapitel \"OER\" beschrieben, stellte dies für uns
einen Kompromiss dar: Die Videos asynchron den Teilnehmenden zur
Verfügung zu stellen, ohne dass sie öffentlich verfügbar sind, da die
Vortragenden dies nicht wollten. \
Dafür war es nützlich, dass sich die Sichtbarkeit der Materialien auf
die Kursteilnehmenden beschränken lässt.\
Der Nachteil ist aber die fehlende öffentliche und dauerhafte
Bereitstellung der Materialien, die über einen gewissen
Nutzer:innenkreis an der TU Dresden hinausgeht. Die Materialien sind an
einen Kurs gebunden, der auch nicht ewig existiert und je nachdem, wie
die Sichtbarkeitseinstellungen gesetzt sind, braucht es einen Account
für OPAL oder einen kryptischen Link, um das entsprechende Video
anzuschauen. Für eine öffentliche Verteilung der Vorlesungen eher
ungeeignet\...\
Eine weitere Schwierigkeit war zudem im ersten Semester, dass Videos,
die direkt in OPAL hochgeladen werden, nicht direkt im Browser gestreamt
werden können, sondern erst auf den eigenen Rechner herunter geladen
werden müssen. Dies ist zwar im Sinne der Wiederverwendbarkeit des
Materials sehr nützlich, aber zum einfachen Anschauen eher unpraktisch,
zumal auf mobilen Endgeräten der Speicherplatzverbrauch oft nicht
gewünscht ist. Gerade mit geringer Internetbandbreite kann der Download
trotz vorheriger Komprimierung der Videos recht lange dauern und
unterwegs u.U. Mobile Daten beanspruchen. Videos im Browser mit
variabler Qualität streamen zu können, ist da auf jeden Fall deutlich
komfortabler. \
\
**Videocampus Sachsen**

Im zweiten Coronasemester (Wintersemester 2020/2021) erblickte eine neue
Videoplattform für sächsische Hochschulen das Licht der Welt:
der *Videocampus Sachsen*. Als aufpolierte Nachfolgerin der verstaubten
und in die Jahre gekommenen Plattform Magma und mit einigen Funktionen,
die man von gängigen Videoplattformen kennt, macht sie einiges richtig.
So wird man beim Aufruf
von [[https://videocampus.sachsen.de/]{.ul}](https://videocampus.sachsen.de/) direkt
mit einer Übersichtsseite der aktuellen Videos begrüßt, kann Videos nach
Kategorien sortieren, direkt nach Videos suchen und öffentliche Videos
(ohne Login) direkt im Browser anschauen. Damit und durch die einfache
Möglichkeit, Videos über Links zu teilen, ist die Videoplattform
prinzipiell auch für Zuschauer:innen ohne Hochschullogin und sogar ohne
Gastaccount geeignet.\
Die im letzten Kapitel angesprochenen Probleme mit dem Hochladen und
Anschauen in OPAL  sollten in Verbindung mit dieser neuen Videoplattform
behoben werden. Nach anfänglichen Schwierigkeiten (bspw. unbekannt-lange
Konvertierungszeiten) konnten wir über Videocampus Sachsen auch recht
gut Videos zum Streamen in den OPAL-Kurs einbinden \[FUßNOTE: Einbetten
Videos\]. Dazu nutzen wir vorrangig sog. versteckte Links. Die Datei ist
demnach nur dann einsehbar, sofern das Publikum den Link zur Datei
besitzt. Der Nutzer:innenkreis beschränkt sich dadurch weiterhin auf
Menschen, die Zugang zum entsprechenden OPAL-Kurs haben. Somit kommen
wir dem angesprochenen Wunsch der meisten Referierenden nach, dass ihre
Videos nicht veröffentlicht werden sollen. Mit dem entsprechenden
Einverständnis der Referierenden wäre aber auch ein eigener \"Kanal\"
für die tuuwi auf Videocampus Sachsen denkbar,  über den wir unsere
Aufzeichnungen bequem hochladen und einem breiteren Publikum zukommen
lassen. Auch eine Einbindung der Videos auf unserer Webseite wäre
darüber möglich. Bleiben nur die Probleme, dass für die Nutzung weiterer
Funktionen, wie dem Kommentieren, ein Hochschullogin erforderlich ist,
Videos sich nicht downloaden lassen und der Nutzer:innenkreis weiterhin
stark auf Hochschulen beschränkt bleibt. \
\
*Hier ein Flowchart zu unserem technischen Vorgehen für synchrone und
asynchrone Leere einfügen:*\
<https://cloudstore.zih.tu-dresden.de/index.php/s/AxPq7beFE74Jwf2>\
\
**Eigene Videoplattform/Webseite**

**Teilen der Videos über die eigene Webseite**

Eine Überlegung war es, die Videos auf unserer bestehenden Webseite
hochzuladen und sie darüber zu teilen. Allerdings ist eine Webseite bzw.
ein Blog eher weniger für die Verwaltung und Verteilung größerer
Videomengen ausgelegt: 

-   Es fehlen Werkzeuge, die speziell auf einen einfachen, häufigen
    Videoupload zugeschnitten sind.

-   Eine automatische Bereitstellung verschiedener Videoqualitäten ist
    nicht gegeben.

-   Das Design der Website ist eher für längere Blogartikel als für eine
    größere Zahl an Videos konzipiert.

-   Es fehlen Suchfunktionen, die speziell auf Videos ausgelegt sind. 

Kurz: ein normaler Blog ist nicht dafür gemacht, eine größere Anzahl
Videos aufzunehmen und Menschen einfach zugänglich zu machen. Dies
schließt allerdings nicht aus, ihn als sekundäres Verbreitungsmedium zu
nutzen! So ist es z. B. möglich, in einen Blogartikel zur aktuellen
Vorlesung die Aufzeichnung einzubinden. Das Video selbst liegt auf einer
externen Plattform, der/die Leser:in muss aber nicht die Website
wechseln, sondern kann das Video direkt im Blogartikel anschauen. \
\
**Eingekaufte Videoplattform**

Zu Beginn des Projekts haben wir uns außerdem eine kommerzielle Lösung
angeschaut. Diese bestand aus einer von einer Firma selbst entwickelten
Videoplattform, welche speziell auf den Einsatz an Hochschulen
zugeschnitten ist. Es bestand dabei die Möglichkeit, eine komplett
eigene Plattform unter einer eigenen Webadresse zur Verfügung gestellt
zu bekommen, zusammen mit dem Hosting, Cloudspeicher für die Videos und
Support und was sonst noch alles dazu gehört. Allerdings hätte das
natürlich eine ordentliche Stange Geld gekostet und zwar nicht nur
einmalig für die Bereitstellung, sondern auch hohe laufende Kosten. Da
unser Projekt auch im Kostenpunkt nachhaltig und einfach mit verfügbaren
Mitteln nachahmbar sein soll, kam dieses Modell für uns nicht in Frage.
Auch die Light-Variante, eine Hochschule, welche diese Plattform bereits
einsetzt und über die wir auch darauf aufmerksam geworden sind, um eine
Mitnutzung zu bitten, haben wir nach weiterer Überlegung verworfen.
Nichts desto trotz war es sehr interessant, sich in dieser Richtung
umzuschauen und auch \'mal ein Telefonat mit der entsprechenden
Marketingabteilung zu verfügbaren Angeboten zu führen. Einerseits um zu
wissen, wie kommerzielle Angebote aussehen können und wie viele diese
tatsächlich kosten. Ganz besonders aber, um unsere Anforderungen an
eingesetzte Plattformen und Software zu schärfen und zu erkennen, was
wir nicht wollen. Das war der Zeitpunkt im Projekt, an dem wir uns ganz
bewusst gegen eine kommerzielle Lösung und für freie Software
entschieden haben. 

**Öffentliche Videoportale**

Eine weitere sehr naheliegende Möglichkeit der Verbreitung sind
öffentliche Videoportale wie Youtube oder Peertube. Die Funktionsweise
solcher Videoportale ist meist recht ähnlich: Es gibt öffentliche Kanäle
mit verschiedenen Themen auf denen Videos geteilt werden können. Jedem
Video kann ein Beschreibungstext hinzugefügt werden und die Videos in
irgendeiner Art in Kategorien eingeordnet werden, damit sie einfacher
gefunden werden. Außerdem gibt es die Möglichkeit, Videos zu bewerten
und in einer Kommentarsequenz die Meinung kundzutun und sich
auszutauschen. 

**Youtube**

Um eine größere Zuschauer:innenzahl zu erreichen ist Youtube derzeit
sicherlich die beste Variante, wobei man auch beachten muss, dass es
mittlerweile schwer ist, aus der großen Masse der täglich neu
hochgeladenen Videos hervorzustechen. Wie gut sich Videos auf Youtube
verbreiten, ist außerdem stark abhängig vom Empfehlungsalgorithmus der
Plattform, an dem Youtube ständig schraubt, für den viele Kriterien eine
Rolle spielen und dessen Vorgehen alles andere als transparent ist. \
Die Verwendung von Youtube für unsere Zwecke sehen wir sehr kritisch,
weswegen wir uns letztendlich dagegen entschieden haben.\
**Youtube ist ein Produkt von Google\...**\
Dies ist in mehrerlei Hinsicht problematisch. Zum einen ist das
Unternehmen profitorientiert und schaltet dafür Werbung vor Videos, die
auf der Plattform geteilt werden. Der Umfang der Werbung wird dabei
regelmäßig ausgeweitet. So laufen mittlerweile teilweise zwei nicht
überspringbare Werbeclips vor Videos und auch in Videos von Kanälen, die
nicht im Youtube-Partnerprogram sind, also nicht an der Werbung vor
ihren eigenen Videos mitverdienen, wird mittlerweile Werbung geschaltet.
\[Fußnote: [[https://www.cnbc.com/2020/11/19/youtube-will-put-ads-on-non-partner-videos-but-wont-pay-the-creators.html]{.ul}](https://www.cnbc.com/2020/11/19/youtube-will-put-ads-on-non-partner-videos-but-wont-pay-the-creators.html)\].
Das ist nicht nur insofern problematisch, dass Menschen durch immer mehr
und immer bessere auf sie zugeschnittene Werbung zu mehr Konsum angeregt
werden, bzw. fragwürdige Firmen vorgestellt bekommen.\
Als Anbieter:in der Plattform erlässt Youtube entsprechende Richtlinien,
welche Inhalte erlaubt sind und welche nicht und setzt diese über
automatische Systeme, aber auch manuelle Sichtung um. Die Richtlinien
sind zwar in den meisten Fällen sehr sinnvoll. Allerdings liegt die
Deutungshoheit eben komplett bei einer profitorientierten Firma und die
Nutzer:innen haben de facto kein Mitbestimmungsrecht an diesen Regeln.
Ein deutlicher Einfluss der Werbeindustrie im Sinne der Vermarktbarkeit
von Videos ist hier zu bemerken. \
So gibt es derzeit vermehrt Berichte, dass rein informative Videos, die
Sachverhalte rund um Hacking erklären, ab 18 geschaltet werden oder
sogar ganze Kanäle mit diesem Themenbezug gesperrt werden, was zu
deutlichen Einschränkungen für die betreffenden Kanälen führt
\[[[https://youtube.com/watch?v=ZiYLPjCWoXU]{.ul}](https://youtube.com/watch?v=ZiYLPjCWoXU)\]. \
Außerdem wird zunehmend auf Algorithmen zur automatischen Löschung von
Videos und Kanälen gesetzt, um die immer größer werdende Menge an
täglich hochgeladenen Videos zu händeln. Wie die Algorithmen genau
arbeiten, ist dabei sehr intrasparent. Dass die Algorithmen außerdem
ebenfalls fehlbar sind, zeigen viele Beispiele fälschlicherweise
gelöschter Videos oder gesperrter Kanäle.
\[Fußnote: [[https://netzpolitik.org/2017/youtube-algorithmen-ersetzen-angeblich-180-000-moderatoren/]{.ul}](https://netzpolitik.org/2017/youtube-algorithmen-ersetzen-angeblich-180-000-moderatoren/)\]\
Ein weiteres sehr großes Problem ist der geringe Datenschutz bzw. die
Vielzahl an Daten, die bei der Nutzung der Plattform gesammelt und mit
anderen Daten von Google verknüpft werden. Diese Sammelwut und
Verwendung der Daten entspricht nicht unserem Verständis einer freien
Gesellschaft. Das ganze Thema zu Datenschutz bei Google und anderen
monopolistischen, profitorientierten Internetunternehmen und welche
Auswirkungen die Datensammelwut auf den einzelnen Menschen und unsere
Geselllschaft als Ganzes hat, würde diesen Leitfaden allerdings
sprengen. Wir empfehlen Dir wärmstens, Dich selbst weiter mit dem Thema
auseinander zu setzen. Ein sehr guter Einstieg ist da zum Beispiel der
Blog des IT-Sicherheitsexperten Maik Kuketz und speziell zu Google sein
Artikel \"Tschüss Datenkrake ein Leben ohne Google\"
\[[[https://www.kuketz-blog.de/tschuess-datenkrake-ein-leben-ohne-google/]{.ul}](https://www.kuketz-blog.de/tschuess-datenkrake-ein-leben-ohne-google/)\].\
Speziell bezogen auf das Teilen von Videos als OER gibt es bei Youtube
ebenfalls das Problem, dass keine Option zum Download von Videos auf der
Plattform selbst angeboten wird. Das erschwert die Möglichkeit der
freien Weiterverwendung im Sinne von OER. Grundsätzlich ist ein Download
von Videos von Youtube über Drittanbietertools möglich, aber nach den
Youtube Richtlinien eher im Graubereich. Im Namen des Copyrights werden
auch häufiger Schritte gegen solche Tools unternommen.
\[Quelle: [[https://github.com/github/dmca/blob/master/2020/10/2020-10-23-RIAA.md]{.ul}](https://github.com/github/dmca/blob/master/2020/10/2020-10-23-RIAA.md)\]
Dies erschwert die Weiterverwendung und erfordert eine eventuelle
Bereitstellung zum Download der eigenen Videos auf einem weiteren
Portal.

**Peertube**

Bei Peertube handelt es um eine freie (im Sinne von freiheitsgewährende)
Alternative zu Youtube. Die Freie Software Peertube wird von der
französischen Non-Profit-Organisation Framasoft entwickelt. Der
Unterschied zu Youtube ist unter anderem, dass es nicht eine zentrale
Stelle gibt, an der alle Videos gesammelt werden, sondern Peertube ein
Netz aus mehr oder weniger unabhängigen Knotenpunkten ist. Jeder Knoten
(Instanz) ist ein eigener Server, auf der Peertube-Software läuft. Pro
Instanz gibt es dabei eigene Accounts, die dort Videos hochladen können,
welche auf dieser Instanz gespeichert sind. Außerdem können die
Betreiber:innen der Instanzen für Ihre eigenen Instanzen eigene
Richtlinien festlegen, welche Videos hochgeladen werden dürfen und
moderieren im besten Fall ihre Instanz auch aktiv, um sicherzustellen,
dass alle Videos ihren Richtlinien entsprechen. Damit man nicht ständig
zwischen den einzelnen Instanzen wechseln muss, um weitere Videos und
Kanäle zu entdecken, können die Instanzen untereinander kommunizieren.
Dadurch ist es möglich, von einer Instanz aus, wie
z.B. [[https://video.dresden.network/]{.ul}](https://video.dresden.network/) Videos
auf anderen Instanzen zu finden, ohne die Webseite wechseln zu müssen.
Dies wird Föderation genannt. Dabei können die Betreiber:innen selbst
wählen, mit welchen anderen Instanzen ihre Instanz föderieren soll. Dies
verhindert z.B., dass Videos von Instanzen mit
fragwürdigen/rechtswidrigen Inhalten auf der eigenen Instanz
vorgeschlagen werden. Aus diesem Grund ist es sehr wichtig, bei der Wahl
einer Instanz darauf zu achten, dass diese klare Richtlinien hat, mit
denen man einverstanden ist und dass diese konsequent umgesetzt werden.\
Diese dezentrale Aufteilung von Peertube in viele einzelne Knoten mit
jeweils eigenen Regeln verhindert, dass eine einzige Firma alle Fäden in
der Hand hält und bestimmt, was auf der Plattform zu sehen bzw. eben
nicht zu sehen ist. Sollte man mit der Moderation bzw. Regeln der
gewählten Instanz unzufrieden sein, hat man jederzeit die Möglichkeit,
den eigenen Videokanal auf eine andere Instanz umzuziehen. \
Zudem hat man auch die Möglichkeit, selber eine Instanz zu betreiben und
somit eigene Regeln festzulegen und z.B. eine Instanz für lokale
Initiativen aufzubauen. Ein weiterer Vorteil ist, dass dadurch die
Werbefreiheit gewährleistet werden kann und man frei von
Profitinteressen einzelner Firmen Videos teilen kann. Die Entwicklung
der Software wird ebenso über Spenden an die Non-Profit-Organisation
Framasoft finanziert, wie auch die meisten Instanzen von privaten
Initiativen und Menschen. \
Auch ist eine thematische Ausrichtung der Instanzen möglich, so ist
z.B. [[https://video.dresden.network/]{.ul}](https://video.dresden.network/) eher
für Vereine und Personen aus Dresden gedacht und es gibt Instanzen, die
z.B. eher einen technischen oder künstlerischen Fokus mit ihren Kanälen
haben. \
Also hör\' dich \'mal in deiner Umgebung um, ob es schon eine thematisch
passende Instanz für deinen Account gibt. \
Eine Überlegung ist/war es auch, für die tuuwi eine eigene
Peertube-Instanz für lokale Initiativen zu hosten, welche speziell auf
das Thema Klimagerechtigkeit ausgerichtet ist. Allerdings stellt dies
natürlich einen erhöhten Aufwand dar, da die Instanz betreut werden muss
und Serverkosten entstehen. Aus diesem Grund werden wir uns
wahrscheinlich doch erstmal für einen Account auf einer bestehenden
lokalen Instanz entscheiden, sobald wir Videos haben, die wir öffentlich
teilen können.\
Weitere Informationen zu Peertube und verschiedenen Instanzen finden
sich hier: [[https://joinpeertube.org]{.ul}](https://joinpeertube.org)

**Fazit**

Als auf den Unialltag spezialisierte Lernplattform mit vielen
entsprechenden Funktionen, ist OPAL super für die Organisation von
Veranstaltungen innerhalb der TUD und anderen Hochschulen geeignet.
Videocampus Sachsen behebt bereits viele Probleme und scheint eine große
Lücke eines bisher fehlenden guten, modernen Videoportals für die TU
Dresden zu schließen. \
Für das Ziel, Umweltbildungsvideos möglichst vielen Menschen ohne große
Hürden bereitzustellen, braucht es allerdings freie zugängliche
Plattformen wie z.B. Peertube. Durch die Verwendung offenerer
Plattformen mit größerer Zielgruppe besteht auch die Chance, dass
Menschen, die keine Verbindung zum Sächsischen Hochschulraum haben,
unsere Videos finden. Nichtsdestotrotz werden wir auch Videocampus
Sachsen weiterhin im Blick behalten und erstmal als Hauptplattform
verwenden. Sobald wir unsere Aufzeichnungen komplett öffentlich zur
Verfügung stellen können, werden wir eher zu Peertube für eine breitere
Zielgruppe wechseln und eventuell Videocampus Sachsen als
Sekundärplattform nutzen. Soweit unsere aktuellen Überlegungen. \
\
**Zusammenfassung aller Punkte anhand einer Mailvorlage zur Absprache
mit Dozierende:**

*Liebe:r \[Referent:in\],*\
\
*vielen Dank, dass Sie uns mit Ihrem Vortrag/Workshop/... in unserer
Vorlesung \[Titel\] unterstützen!*\
*Letztes Semester haben wir Bildungsangebote komplett online angeboten
(sowohl synchron, als auch asynchron) und haben schon diverse
Erfahrungen mit der digitalen Umstellung gemacht - wahrscheinlich so wie
Sie auch. Dieses Semester bieten wir diese Vorlesung ebenfalls online
an. Dazu können Sie verschiedene Tools nutzen, um Ihren Input
aufzuzeichnen. Es gibt folgende Möglichkeiten:*\
\
*[Asynchrone Lehre (aufgezeichnete Vorträge bzw. Präsentationen)]{.ul}*

-   *Sie nehmen im Voraus Ihren Input auf.*

-   *Dazu haben wir mit der Open-Source-Software OBS sehr gute
    Erfahrungen gemacht und dafür auch einen Schritt-für-Schritt
    Leitfaden erstellt, den wir Ihnen zukommen lassen können.*

-   *Sie laden dann Ihren Vortrag in einer von uns zur Verfügung
    gestellten Cloud hoch und wir kümmern uns um die Verbreitung unter
    den Studierenden auf OPAL (Online-Kurs-Tool der TU Dresden).*

-   *Vorteil: Sie können in Ruhe Ihren Input vor- und nachbereiten,
    Nachteil: es fehlen der Austausch und die Möglichkeit der
    Fragestellung auf Seiten der Studierenden. *

-   *Unsere Lösungsmöglichkeiten: *

    -   *Wir sammeln Fragen der Studis nach dem Hochladen Ihres Inputs
        und senden Ihnen diese zu, die Sie dann gesammelt beantworten.*

    -   *Wir erstellen in OPAL ein Forum, worin die Studis im Laufe
        einer bestimmten Zeit Fragen stellen und Sie diese als Gast
        beantworten können.*

    -   *Fällt Ihnen noch was ein?*

*[Synchrone Lehre (Live-Veranstaltungen)]{.ul}*

-   *Sie gestalten Live-Vorlesungen.*

-   *Dazu haben wir unsere eigene BigBlueBotton-Instanz
    (Video-Chat-Konferenz-Tool), welche unglaublich viele Features zur
    Vorlesungsgestaltung bereithält (Umfragen, Chat-Funktion,
    „Raum"Aufteilungen etc.), sodass auch Workshops, World-Cafes oder
    andere Kleingruppenformate möglich sind.*

-   *Auch hier können Sie Ihre Präsentation einpflegen. *

-   *Diese Live-Sendung nehmen wir trotzdem auf, damit wir diese allen
    Studis bereitstellen können.*

-   *Hier lohnt sich ein vorheriger Testlauf, um sich mit der Instanz
    und dessen Möglichkeiten auseinanderzusetzen und sie kennenzulernen.
    Hierzu können wir gern einen Termin ausmachen. Einen Leitfaden
    können wir Ihnen ebenso zukommen lassen.*

-   *Vorteil: direkter Kontakt mit dem Publikum, Nachteil: gute
    technische Voraussetzungen sind nötig (Headsets, sehr gute
    Internetverbindung etc.)*

*Wenn Sie noch mit anderen Tools oder Formaten (wie Podcasts und Co.)
gute Erfahrungen gemacht haben, teilen Sie uns dies gern mit. Wichtig
ist uns eine Aufzeichnungsmöglichkeit -- egal ob live oder nicht, damit
alle Studierenden auch in Corona-Zeiten immer Zugang zu Ihrem Input
haben.*\
\
*Dazu möchten wir diese Aufzeichnung veröffentlichen. Dies ist zum einen
auf der TUD-internen Lernplattform OPAL möglich, wo nur das Publikum
Zugriff darauf hat. Weiterhin beschäftigen wir uns schon seit langem mit
dem OpenEducationalRessources Ansatz und möchten Wissen niedrigschwellig
und frei zugänglich teilen. Deshalb können Sie auch einer
Veröffentlichung dahingehend zustimmen. Lesen Sie sich dazu die
Einverständniserklärungen und den OER Leitfaden im Anhang durch. *\
\
*Überlegen Sie sich also gern *\
*a) was für ein Vorlesungsformat Sie wählen (asynchron oder synchron,
Vortrag, Podcast, Workshop usw.) und *\
*b) angelehnt daran, welche Tools Sie zur optimalen Umsetzung nutzen
möchten und wir klären dann die folgenden Schritte. *\
*c) inwieweit sie einer (freien) Veröffentlichung zustimmen.*\
\
*Bei weiteren Fragen oder Ideenvorschlägen melden Sie sich gern
zurück.*\
*Ganz liebe Grüße,*\
*\[Name\]*\
\