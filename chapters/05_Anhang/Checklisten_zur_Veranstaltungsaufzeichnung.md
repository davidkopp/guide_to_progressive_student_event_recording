# 5.1 Checkliste

**Präsenz**\
1. Vorbereitung

-   Einverständniserklärungen erstellen und einholen

    -   Alles steht und fällt mit der Einverständniserklärung, diese
        sollte mit Bedacht formuliert und gut vorbereitet sein. Es
        empfiehlt sich sehr, rechtliche Hilfe hinzu zu ziehen, wenn man
        sich nicht ganz sicher ist.

-   zum Filmen benötigtes Material ausleihen/besorgen

    -   Kamera, Stativ, Mikrophon Desktoprecorder (empfehlenswert),
        Verlängerungskabel, Speicherkarten, Batterien

-   Raum vorbereiten

    -   rechtzeitig im Raum sein

    -   Steckdosen auschecken

    -   Lichtverhältnisse checken

    -   Speicherplatz und Batterien kontrollieren

-   Studis/ Publikum über Aufzeichnung aufklären

2\. Durchführung

-   Filmen

    -   rechtzeitiger Beginn der Aufzeichnung (lieber früher, denn das
        kann man wegschneiden)

    -   Synchronisation von Audio und Bild (vor Start der Veranstaltung
        vor der Kamera kurz schnipsen oder leise klatschen)

    -   regelmäßige Technikkontrolle

    -   Kameraausrichtung beachten (die vortragende Person sollte immer
        im Bild zu sehen sein)

    -   Fragen aus dem Publikum notieren

3\. Aufbereitung

-   Schneiden

    -   Bild- und Tonspur übereinanderlegen

    -   unnötiges Material entfernen

-   Bearbeiten

    -   Dateigröße komprimieren

    -   Titelbild, Untertitel und Co. einfügen

-   beide Schritte zügig vollziehen, für eine zeitnahe Veröffentlichung

4\. Veröffentlichung

-   Einverständniserklärungen der Referierenden beachten

    -   interne oder freie Veröffentlichung

    -   versteckte oder öffentliche Links

-   Konvertierungszeit beachten (Ankündigung der Veröffentlichung lieber
    1-2 Tage später)

**Online**\
1. Vorbereitung

-   Absprache Referierende

    -   Mensch sollte die Referent:innen vorher über die Möglichkeiten
        informieren (z.B. synchrone oder asynchrone Lehre, Tools,
        Aufzeichnungen\...)

    -   Pre-Test vereinbaren (eigenständiger Termin oder 20min vor
        eigentlicher Veranstaltung)

-   Einverständniserklärungen erstellen und einholen

    -   Alles steht und fällt mit der Einverständniserklärung, diese
        sollte also bedacht formuliert und gut vorbereitet sein. Es
        empfiehlt sich sehr, rechtliche Hilfe hinzu zu ziehen, wenn man
        sich nicht ganz sicher ist.

-   digitalen Raum vorbereiten und kommunizieren

    -   die Studies sollten rechtzeitig und ein zweites Mal kurz vorher
        über den Raum informiert werden

    -   ebenso die Referierenden

2\. Durchführung

-   kurz vorher

    -   Studis/ Publikum über Aufzeichnung aufklären: \"Alle die nicht
        auf der Aufzeichnung zu sehen sein wollen, lassen ihre Kamera
        aus und melden sich bei Wortmeldung per Chat\"

    -   eigene Aufzeichnung vorbereiten (sicherheitshalber empfiehlt es
        sich, eine eigene Aufnahme des Vortrags zu machen)

-   Aufzeichnung

    -   rechtzeitiger Beginn der Aufzeichnung (lieber früher, denn das
        kann man wegschneiden)

    -   regelmäßige Technikkontrolle

3\. Aufbereitung

-   Schneiden

    -   Bild- und Tonspur übereinanderlegen

    -   unnötiges Material entfernen

-   Bearbeiten

    -   Dateigröße komprimieren

    -   Titelbild, Untertitel und Co. einfügen

-   beide Schritte zügig vollziehen, für eine zeitnahe Veröffentlichung

4\. Veröffentlichung

-   Einverständniserklärungen der Referierenden beachten

    -   interne oder freie Veröffentlichung

    -   versteckte oder öffentliche Links

-   Konvertierungszeit beachten (Ankündigung der Veröffentlichung lieber
    1-2 Tage später)