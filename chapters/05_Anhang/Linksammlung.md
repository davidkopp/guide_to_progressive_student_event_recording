**Didaktik**\
\...um ein bisschen die Online-Lehre aufzupeppen:

-   [[https://www.oncoo.de/]{.ul}](https://www.oncoo.de/)

    -   Oncoo ermöglicht unter anderem Kartenabfragen,
        Zielscheibenfeedback usw.

-   [[https://www.tricider.com/home;jsessionid=wHVClVRujDpU1kO-qhsEcw]{.ul}](https://www.tricider.com/home;jsessionid=wHVClVRujDpU1kO-qhsEcw)

    -   Tricider ermöglicht es u.a., Fragen zu stellen und dafür
        Argumente und Vorschläge zu sammeln.

-   [[https://answergarden.ch/]{.ul}](https://answergarden.ch/)

    -   Wortwolken, Assoziationen sammeln

-   [[https://tweedback.de/]{.ul}](https://tweedback.de/)

    -   Live-Feedback

Weiterhin könnt ihr natürlich die in den Videochatsoftwares vorhandenen
Funktionen nutzen. Wie bspw. in BBB die Umfragefunktion, das Whiteboard
in Mehrbenutzermodus usw.\
\
Mehr Tools findest du u.a. hier:

-   [[https://hackmd.io/\@m3lgTmR3Q-ugT7xywwUsJw/HyU0s0dM7\#Kollaborative-Materialsammlung]{.ul}](https://hackmd.io/@m3lgTmR3Q-ugT7xywwUsJw/HyU0s0dM7#Kollaborative-Materialsammlung)

-   [[https://wbdig.guetesiegelverbund.de/tools]{.ul}](https://wbdig.guetesiegelverbund.de/tools) mit
    Tool-o-search

**Webseiten**

-   [[https://open-educational-resources.de/]{.ul}](https://open-educational-resources.de/)

    -   Informationseite zu Open Educational Resources

-   [[https://creativecommons.org/licenses/]{.ul}](https://creativecommons.org/licenses/)

    -   Informationsseite zu den verschiedenen Creative Commons Lizenzen

\* Trifft es das, was ihr sagen wollt?

\* wird das viertel/dreiviertel irgendwie graphisch umgesetzt?
