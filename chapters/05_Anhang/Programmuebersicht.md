# 5.2 Programmübersicht

In diesem Abschnitt findet sich eine Liste der Tools, die wir im Laufe
unserer Arbeit verwendet haben. \
Sofern nicht anders gekennzeichnet, handelt es sich hierbei um freie
Software bzw. OpenSource Software.

## Verwaltung/Teilen von Dateien/Dokumenten

-   [**Humhub**](https://www.humhub.com/de)

    -   umfangreiche Plattform zur Zusammenarbeit in Gruppen

    -   beschreibt sich selbst als \"Social Network Kit\", da es vom
        Aufbau und der Bedienung mit Widgets und Timelines an ein
        soziales Netwerk erinnert

    -   bietet an Funktionen u. a. gemeinsames Wiki, Dateiverwaltung,
        Kalender und kann über Plugins stark erweitert werden

    -   bildet das Herzstück der digitalen Zusammenarbeit der tuuwi, in
        dem wir dort Termine eintragen, Bilder und Protokolle
        archvieren, Dateien verwalten

    -   in der DF vor allem für die Wissenspeicherung, Dokumentierung
        und Aufgabenverwaltung verwendet 

-   [**Nextcloud**](https://nextcloud.com/)

    -   Software zur Erstellung von Cloudspeichern

    -   für Studierende/Mitarbeitende/Hochschulgruppen bietet die TU
        Dresden eine eigene Instanz unter cloudstore.zih.tu-dresden.de
        an

    -   als tuuwi haben wir dort einen eigenen Cloudspeicher bekommen,
        wie wir diesen verwendet haben, wird im Kapitel \"Kommunikation
        mit den Referierenden\" beschrieben 

-   [**Etherpad Lite**](https://etherpad.org/)

    -   Software für das schnelle und einfache kollaborative Schreiben
        von Dokumenten direkt im Browser

    -   als tuuwi betreiben wir eine eigene Instanz

    -   Vorstellung/Anleitung: [https://tuuwi.de/2020/04/04/einfach-gemeinsam-an-texten-arbeiten-pads-machen-es-moeglich/](https://tuuwi.de/2020/04/04/einfach-gemeinsam-an-texten-arbeiten-pads-machen-es-moeglich/)


-   [**OnlyOffice**](https://www.onlyoffice.com/)

    -   Möglichkeit Office Dokumente direkt im Browser mit einer großen
        Bandbreite zu bearbeiten 

    -   z\. B. zur Erstellung des Leitfadens genutzt

    -   nützlich, wenn ein festes Layout und mehr Bearbeitungstools als
        in Etherpad Lite benötigt werden

    -   der StuRa der TU Dresden stellt eine eigene Instanz bereit, die
        wir verwendet haben

-   [**Lufi (ähnlich zu Firefox Send)**](https://framagit.org/fiat-tux/hat-softwares/lufi)

    -   Browsertool zum einfachen Teilen von verschlüsselten Dokumenten

    -   vor allem nützlich, wenn Dokumente übersendet werden, die
        persönliche Informationen enthalten 

    -   Dokumente werden direkt im Browser verschlüsselt und nur
        verschlüsselt auf dem Server gespeichert

    -   Es lässt sich unter anderem eine Ablaufzeit einstellen, zu der
        die Dokumente nicht mehr einsehbar sind

    -   Am Ende hat man einen Downloadlink, den man einfach teilen kann 

    -   kann mit wenig Aufwand selber gehostet werden

    -   Mögliche Instanz, die verwendet werden
        kann: [https://upload.disroot.org/](https://upload.disroot.org/)

## Videoaufzeichnung/-bearbeitung

-   [**OBS (Open Broadcaster Software)**](https://obsproject.com/)

    -   umfangreiche Aufzeichnungssoftware, die das Aufzeichnen des
        Bildschirms und Tons ermöglicht

    -   hauptsächlich für die Asynchrone URV in den Coronasemestern
        verwendet

    -   weitere Informationen im OBS-Leitfaden (Anh. x)

-   [**Shotcut**](https://shotcut.org/)

    -   einfaches Videoschnittprogramm

    -   Aufgrund der Aufzeichnung mit OBS in den Coronasemestern, welche
        bereits viele Möglichkeiten, wie z.B. das Erstellen einer Szene
        mitbringt, benötigten wir erstmal keine Schnittsoftware 

    -   sobald wieder Präsenzvorlesungen mit Aufnahme möglich sind, wird
        aber definitiv ein Schnittprogramm benötigt, um z.B. die Video-
        und Tonspuren zusammenzuführen

-   [**Handbrake**](https://handbrake.fr/)

    -   Software zur Komprimierung von Videos, d.h. um sie vor dem
        Upload zu verkleinern 

    -   sehr einfache Handhabung und erzeugt ohne große Einstellung
        schon deutlich kleinere Videos, ohne merkbaren Qualitätsverlust

## Veröffentlichung

-   [**OPAL**](https://bildungsportal.sachsen.de/opal/)

    -   Lernplattform des Bildungsportals Sachsen

    -   verwendet für Onlineeinschreibung in die URV, Kontakt zu den
        eingeschriebenen Studierenden und im digitalen Semester zur
        Veröffentlichung der Vorlesungsvideos in geschlossenem Rahmen

-   [**Videocampus Sachsen**](https://videocampus.sachsen.de/)

    -   Videoportal des Bildungsportals Sachsen

    -   ermöglicht das überwiegend hochschulinterne Teilen von Videos 

    -   ist an OPAL angebunden

-   [**Peertube**](https://joinpeertube.org/) 

    -   dezentrale Videoplattform

    -   freie Alternative zu Youtube ohne Werbung

    -   kann mittlerweile auch Livestreaming

    -   entwickelt durch die Non-Profit-Organsiation Framasoft

## Kommunikation

-   [**Telegram**](https://telegram.org/)

    -   für die interne Kommunikation des Projektteams haben wir,
        aufgrund der starken Etablierung des Messengers innerhalb der
        tuuwi, Telegram verwendet

    -   Features, wie Terminabstimmungen, Fileupload,
        Sprach/Videonachrichten haben uns die Kommunikation sehr
        erleichtert

    -   aus Datenschutz und Sicherheitssicht ist Telegram allerdings
        weniger zu empfehlen, da nicht gerade datensparsam und Ende zu
        Ende Verschlüsselung in Gruppenchats nicht möglich
        ist: [[https://www.kuketz-blog.de/telegram-sicherheit-gibt-es-nur-auf-anfrage-messenger-teil3/]](https://www.kuketz-blog.de/telegram-sicherheit-gibt-es-nur-auf-anfrage-messenger-teil3/)

        -    bessere Alternativen wären z.B. Signal, Threema oder Matrix

    -   nur die Clientanwendungen (App, Webanwendung und
        Desktopanwendung) von Telegram sind OpenSource, der Servercode
        ist nicht einsehbar

-   **Email**

    -   für den Kontakt zu Referierenden, offiziellen Stellen und der
        Verwaltung ist weiterhin Email das Mittel der Wahl

    -   für die Kommunikation per E-Mail haben wir meist unsere
        offiziellen E-Mail-Adressen der TU Dresden verwendet

-   **weitere Tools**

    -   [dudle](https://dudle.inf.tu-dresden.de/)
        (Terminabstimmungstool der TU-Dresden)

    -   [Matrix](https://doc.matrix.tu-dresden.de) (umfassendes
        Chat-Tool)

## Konferenzsoftware
-   [**BigBlueButton (BBB)**](https://bigbluebutton.org/)

    -   Konferenzsoftware

    -   ermöglicht einfache digitale Treffen 

    -   sowohl zur Teambesprechung als auch für Livesessions der URV
        genutzt

    -   größerer Funktionsumfang

    -   im Laufe der Pandemie ebenfalls von der TU bereitgestellt, lange
        Zeit haben wir aber auch eine eigene Instanz verwendet

-   **GoToMeeting**

    -   da im zweiten Coronasemester sehr viele Menschen an den URV
        teilgenommen haben und wir wieder ein Liveformat an bieten
        wollten, mussten wir dafür leider auf diese beiden proprietären
        Anwendungen ausweichen =\> für über 200 Menschen in einem Raum,
        haben die uns zur Verfügung stehenden BBB Instanzen leider nicht
        ausgereicht

    -   **Proprietär und Serverstandort in den USA**

    -   wenn immer möglich, empfehlen wir die Nutzung von freien
        Alternativen wie z.B. BBB