# \"Wo muss ich da jetzt klicken?\" - Ein Leitfaden zur progressiven studentischen Veranstaltungsaufzeichnung mit Theorie und Praxis

## Mit Inputs zur studentischen Selbstorganisation, freier Software, OER und praktischer Umsetzung.

## Aktueller Hinweis
Schön, dass du hierher gefunden hast. Aktuell befindet sich dieses Repo noch im Aufbau und die Texte aus der Broschüre müssen noch fertig eingepflegt werden. Auf Grund der laufenden Prüfungszeit, wird sich das noch etwas verzögern. 

## Mithelfen und Verbessern
Wir möchten nicht, dass unser Leitfaden einfach nur den Istzustand zum Zeitpunkt unseres Projekts darstellt und dann als PDF irgendwo auf der Festplatte verstaubt. So wie sich unserere Sichtweise und unser Wissenstand ändert und erweitert, so ändern sich auch die Prozesse/Programme, die wir im Leitfaden beschrieben haben.
Aus diesem Grund möchten wir gerne allen die Möglichkeiten geben sich an der Aktualisierung und Erweiterung des Leitfadens zu beteiligen. Dadurch kann der Leitfaden von einer reinen auf unsere Sichtweise beschränkten Momentaufnahme, zu einem offenen und sich ständig weiterentwickelnden Wissenssammlung und Erfahrungsbericht für alle werden.

Um dir und uns die Möglichkeit zu geben, den Text strukturiert bearbeiten zu können und eine klare Versionierung sicher zu stellen, haben wir für den Leitfaden dieses Repo erstellt. 

In Zukunft soll es möglich sein über Pullrequests Änderungen bzw. Ergänzungen vorzuschlagen, die wir dann einpflegen.

## Inhaltsverzeichnis

1.  [**Vorwort**](./chapters/01_Vorwort.md)

    1.  Wer sind wir? Was wollen wir? Oh man, was passiert?

    2.  Interview zwischen DF und tuuwi

2.  [**THEORIE: Streitschrift für freies Wissen, freie Software und
    studentische Selbstorganisation**](./chapters/02_Theorie.md)

    1.  Studentische Selbstverwaltung - (fehlende) Grundlagen des
        Arbeitens

    2.  freies Wissen - OER

        1.   Lizenzierung - Creative Commons

        2.  Schwierigkeiten

    3.  freie Software - Open Source Programme

3.  [**PRAXIS: Leitfaden zur studentischen Veranstaltungsaufzeichnung**](./chapters/03_Praxis.md)

    1.  Einleitung

    2.  Kommunikation mit Referierenden

    3.  Aufnahme der Veranstaltung

    4.  Bearbeitung des Materials

    5.  Veröffentlichung des Materials

4.  [**Fazit und Lessons Learned**](./chapters/04_Fazit_und_Lessons_Learned.md)

    1.  Zusammenfassung

    2.  Lessons Learned

    3.  Forderungen

5.  **Anhang**

    1.  [Checklisten zur Veranstaltungsaufzeichnung](./chapters/05_Anhang/Checklisten_zur_Veranstaltungsaufzeichnung.md)

    2.  [Programmübersicht](./chapters/05_Anhang/Programmuebersicht.md)

    3.  Vorlagen 

        1.  Einverständniserklärungen

        2.  OER Leitfaden (Bild)

        3.  BBB Leitfaden

        4.  Autor:innenvertrag

## Impressum
2021 TU-Umweltinitiative (tuuwi)\
\
![tuuwi-logo](./logos/tuuwi.png)

**Herausgeberin**: TU-Umweltinitiative (tuuwi) der TU Dresden\
**Autor:innen**: Kai Witza, Cornell Ziepel, Jennifer Vaupel\
**Umschlaggestaltung, Illustration, Layout**: ?\
**weitere Mitwirkende**: ?\
\
Das Werk, einschließlich seiner Teile, ist unter der Creative Commons
Lizenz CC BY- SA 4.0 veröffentlicht \[LOGO\]. \
D.h. ihr dürft das Material in jedwedem Format oder Medium
vervielfältigen und weiterverbreiten sowie remixen, verändern und darauf
aufbauen - alles definitiv wünschenswert!\
Folgende Bedingungen solltet ihr aber dabei beachten:

-   Namensnennung

-   Weitergabe unter gleichen Bedingungen

Das Werk ist im Rahmen des Digital-Fellowship-Programms im
Förderzeitraum 2019/2020 des Hochschuldidaktischen Zentrums Sachsen
(HDS) und des Arbeitskreis E-Learning der LRK Sachsen (AK E-Learning)
entstanden. \[LOGO?\]\
\
Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage
des von den Abgeordneten des Sächsischen Landtages beschlossenen
Haushaltes. \[LOGO Sachsen\]
<br></br>